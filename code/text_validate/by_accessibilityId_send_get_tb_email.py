"""
Tests Various Text views of the Sample Application

"""
from time import sleep
from appium import webdriver
from appium.webdriver.common.mobileby import MobileBy as MBy


def set_desired_caps():
    """."""
    desired_caps = {}
    desired_caps['platformName'] = 'Android'
    desired_caps['platformVersion'] = '9'
    desired_caps['deviceName'] = 'emulator-5554'
    desired_caps['appPackage'] = 'com.mayankjohri.demo.mayaappiumtextboxesdemo'
    desired_caps['appActivity'] = '.MainActivity'
    return desired_caps


def main():
    """."""
    ids = {
        "user_mail": {
            "strategy": MBy.ACCESSIBILITY_ID,
            "val": "Users Email"
        }
    }
    try:
        driver = webdriver.Remote('http://localhost:4723/wd/hub',
                                  set_desired_caps())

        # print("Sleeping...");sleep(5); print("Waking now...")
        ele_name = "user_mail"
        print("Lets get the textbox {ele_name}".format(ele_name))

        txt_box = driver.find_element(ids[ele_name]['strategy'],
                                      ids[ele_name]['val'])

        # Read existing text from text box
        print(txt_box.text)

        # Clear any existing text
        txt_box.clear()

        # Send text to textbox

        txt_box.send_keys("mayankjohri@godsent.com")
        # Gotcha protector:
        sleep(2)
        print(txt_box.text)
    except Exception as e:
        print(e)
    finally:
        if 'driver' in locals():
            driver.quit()


main()
