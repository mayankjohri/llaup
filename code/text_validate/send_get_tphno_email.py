"""
Tests Various Text views of the Sample Application

"""
from appium import webdriver


def set_desired_caps():
    """."""
    desired_caps = {}
    desired_caps['platformName'] = 'Android'
    desired_caps['platformVersion'] = '9'
    desired_caps['deviceName'] = 'emulator-5554'
    desired_caps['appPackage'] = 'com.mayankjohri.demo.mayaappiumtextboxesdemo'
    desired_caps['appActivity'] = '.MainActivity'
    return desired_caps


def main():
    try:
        WD = webdriver.Remote('http://localhost:4723/wd/hub',
                              set_desired_caps())
        p = "02164702130"
        element = WD.find_element_by_name(p)
        # Read existing text from text box
        print(element.text)

        # Clear any existing text
        element.clear()

        # Send text to textbox
        element.send_keys("0751235621")
        print(element.text)
    except Exception as e:
        print(e)
    finally:
        if WD:
            WD.quit()


main()
