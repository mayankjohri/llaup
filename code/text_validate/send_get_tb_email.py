"""
Automate Email Text views interaction
"""
from appium import webdriver


def set_desired_caps():
    """Lists desired capabilities"""
    desired_caps = {}
    desired_caps['platformName'] = 'Android'
    desired_caps['platformVersion'] = '9'
    desired_caps['deviceName'] = 'emulator-5554'
    desired_caps['appPackage'] = 'com.mayankjohri.demo.mayaappiumtextboxesdemo'
    desired_caps['appActivity'] = '.MainActivity'
    return desired_caps


def main():
    """Main Automation Function"""
    try:
        driver = webdriver.Remote('http://localhost:4723/wd/hub',
                                  set_desired_caps())
        print("Lets get the textbox `textView`")
        et_email = "com.mayankjohri.demo.mayaappiumtextboxesdemo:id/et_email"
        element = driver.find_element_by_id(et_email)
        # Read existing text from text box
        print(element.text)

        # Clear any existing text
        element.clear()

        # Send text to textbox
        element.send_keys("arya@world.com")
        print(element.text)
    except Exception as error:
        print(error)
    finally:
        if driver:
            driver.quit()


main()
