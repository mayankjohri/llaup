"""
Tests Various Text views of the Sample Application

"""
from time import sleep
from appium import webdriver

def set_desired_caps():
    desired_caps = {}
    desired_caps['platformName'] = 'Android'
    desired_caps['platformVersion'] = '9'
    desired_caps['deviceName'] = 'emulator-5554'
    desired_caps['appPackage'] = 'com.mayankjohri.demo.mayaappiumtextboxesdemo'
    desired_caps['appActivity'] = '.MainActivity'
    return desired_caps

try:
    driver = webdriver.Remote('http://localhost:4723/wd/hub', set_desired_caps())
    print("Sleeping..."); sleep(5); print("Waking up now...")
except Exception as e:
    print(e)
finally:
    if driver:
        driver.quit()
