"""
Tests Various Text views of the Sample Application

"""
from time import sleep
from appium import webdriver
from appium.webdriver.common.mobileby import MobileBy as MBy


def set_desired_caps():
    """."""
    desired_caps = {}
    desired_caps['platformName'] = 'Android'
    desired_caps['platformVersion'] = '9'
    desired_caps['deviceName'] = 'emulator-5554'
    desired_caps['appPackage'] = 'com.mayankjohri.demo.buttonsdemo'
    desired_caps['appActivity'] = '.MainActivity'
    return desired_caps


def main():
    """."""
    # ids = {
    #     "chkboxs": {
    #         "strategy": MBy.ACCESSIBILITY_ID,
    #         "val": "Users Email"
    #     }
    # }
    try:
        driver = webdriver.Remote('http://localhost:4723/wd/hub',
                                  set_desired_caps())

        # print("Sleeping...");sleep(5); print("Waking now...")
        ele_name = "com.mayankjohri.demo.buttonsdemo:id/textview"
        print("Lets get the textbox {ele_name}".format(ele_name=ele_name))

        ele = driver.find_element_by_id(ele_name)
        ele.click()
        sleep(1)
        tost = driver.find_element_by_xpath("//android.widget.Toast[0]")
        print(tost)
        # print(ele.text, ele.is_enabled(), ele.is_selected(), ele.size)


        # ele.set_text("Done testing")
        sleep(5)
    except Exception as e:
        print(e)
    finally:
        if 'driver' in locals():
            driver.quit()
        print("Bye Bye")


main()
