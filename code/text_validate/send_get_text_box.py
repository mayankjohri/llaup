"""
Tests Various Text views of the Sample Application

"""
from time import sleep
from appium import webdriver


def set_desired_caps():
    """."""
    desired_caps = {}
    desired_caps['platformName'] = 'Android'
    desired_caps['platformVersion'] = '9'
    desired_caps['deviceName'] = 'emulator-5554'
    desired_caps['appPackage'] = 'com.mayankjohri.demo.mayaappiumtextboxesdemo'
    desired_caps['appActivity'] = '.MainActivity'
    return desired_caps


def main():
    """."""
    ids = {
        "name":
            "com.mayankjohri.demo.mayaappiumtextboxesdemo:id/et_Name"
    }
    try:
        driver = webdriver.Remote('http://localhost:4723/wd/hub',
                                  set_desired_caps())
        print("Lets get the textbox `et_Name`")

        txt_box = driver.find_element_by_id(ids['name'])

        # Read existing text from text box
        print(txt_box.text)

        # Clear any existing text
        txt_box.clear()

        # Send text to textbox

        txt_box.send_keys("Ja, Ich bin \tIndisch...")
        # Gotcha: Single line text and escape characters.
        print(txt_box.text)
        sleep(5)
    except Exception as e:
        print(e)
    finally:
        if driver:
            driver.quit()


main()
