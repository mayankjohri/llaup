"""
Tests Various Text views of the Sample Application

"""
from time import sleep
from appium import webdriver
from appium.webdriver.common.mobileby import By


def set_desired_caps():
    """."""
    desired_caps = {}
    desired_caps['platformName'] = 'Android'
    desired_caps['platformVersion'] = '9'
    desired_caps['deviceName'] = 'emulator-5554'
    desired_caps['appPackage'] = 'com.mayankjohri.demo.mayaappiumtextboxesdemo'
    desired_caps['appActivity'] = '.MainActivity'
    return desired_caps


def main():
    """."""
    ids = {
        "password_numeric": {
            "strategy": By.XPATH,
            "val": "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.EditText[3]"
        }
    }

    try:
        driver = webdriver.Remote('http://localhost:4723/wd/hub',
                                  set_desired_caps())

        # print("Sleeping...");sleep(5); print("Waking now...")
        print("Lets get the textbox `et_Name`")
        ele = {}
        for key, val in ids.items():
            ele[key] = driver.find_element(val['strategy'],
                                           val['val'])
            # Read existing text from text box
            print(ele[key].text)

            # Clear any existing text
            ele[key].clear()

            # Send text to textbox

            ele[key].send_keys("0191922")
            # Gotcha protector:
            sleep(2)
            print(ele[key].text)
    except Exception as e:
        print(e)
    finally:
        if driver:
            driver.quit()


main()
