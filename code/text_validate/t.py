"""
Tests Various Text views of the Sample Application

"""
import os
from time import sleep
from appium import webdriver


def set_desired_caps():
    desired_caps = {}
    desired_caps['platformName'] = 'Android'
    desired_caps['platformVersion'] = '4.4.2'
    desired_caps['deviceName'] = '0123456789ABCDEF'
    desired_caps['appPackage'] = 'com.mayankjohri.demo.mayademoapp'
    desired_caps['appActivity'] = '.MainActivity'
    return desired_caps

driver = webdriver.Remote('http://localhost:4723/wd/hub', set_desired_caps())

sleep(10)
driver.quit()
