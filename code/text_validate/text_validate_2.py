"""
Tests Various Text views of the Sample Application

"""
from appium import webdriver


def set_desired_caps():
    """."""
    desired_caps = {}
    desired_caps['platformName'] = 'Android'
    desired_caps['platformVersion'] = '9'
    desired_caps['deviceName'] = 'emulator-5554'
    desired_caps['appPackage'] = 'com.mayankjohri.demo.mayaappiumtextboxesdemo'
    desired_caps['appActivity'] = '.MainActivity'
    return desired_caps


def main():
    try:
        WD = webdriver.Remote('http://localhost:4723/wd/hub',
                              set_desired_caps())
        print("Lets get the textbox `textView`")
        tw = "com.mayankjohri.demo.mayaappiumtextboxesdemo:id/textView"
        txt_box = WD.find_element_by_id(tw)
        print(txt_box.text)
    except Exception as e:
        print(e)
    finally:
        if WD:
            WD.quit()


main()
