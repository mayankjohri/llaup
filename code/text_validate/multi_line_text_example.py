# -*- coding: utf-8 -*-
"""
Tests Various Text views of the Sample Application

"""
from time import sleep
from appium import webdriver
from selenium.webdriver.common.keys import Keys


def set_desired_caps():
    """."""
    desired_caps = {}
    desired_caps['platformName'] = 'Android'
    desired_caps['platformVersion'] = '9'
    desired_caps['deviceName'] = 'emulator-5554'
    desired_caps['appPackage'] = 'com.mayankjohri.demo.mayaappiumtextboxesdemo'
    desired_caps['appActivity'] = '.MainActivity'
    return desired_caps


def main():
    """."""
    ids = {
        "multi_line":
            "com.mayankjohri.demo.mayaappiumtextboxesdemo:id/et_multi_line",
        "et_date":
            "com.mayankjohri.demo.mayaappiumtextboxesdemo:id/et_date"
    }

    try:
        driver = webdriver.Remote('http://localhost:4723/wd/hub',
                                  set_desired_caps())
        print("Lets get the textbox `et_Name`")

        txt_box = driver.find_element_by_id(ids['multi_line'])

        # Read existing text from text box
        print(txt_box.text)

        # Clear any existing text
        txt_box.clear()

        # Send text to textbox
        # driver.hide_keyboard()
        txt_box.send_keys("""Om Bhuuh Om Bhuvah Om Svah
Om Mahah Om Janah Om Tapah Om Satyam
Om Tat-Savitur-Varennyam Bhargo Devasya Dhiimahi
Dhiyo Yo Nah Pracodayaat |
Om Aapo Jyotii Raso[a-A]mrtam Brahma Bhuur-Bhuvah Svar-Om ||""")
        # Gotcha: Multi line text and escape characters.
        print("text sent:", txt_box.text)
        sleep(5)
        print("sending new line")
        txt_box.send_keys("\n\n\n\n\n")
        print("sending yes, I am Indian")
        txt_box.send_keys("Ja, Ich bin Indisch...")
        print(txt_box.text)
    except Exception as e:
        print(e)
    finally:
        if driver:
            driver.quit()


main()
