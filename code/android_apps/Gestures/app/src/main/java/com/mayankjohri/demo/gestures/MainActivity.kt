package com.mayankjohri.demo.gestures

import android.app.Activity
import android.os.Bundle
import android.util.Log
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import com.github.nisrulz.sensey.Sensey
//import com.github.nisrulz.sensey.Sensey.*Senseyß
import com.mayankjohri.demo.gestures.R.*
import com.mayankjohri.demo.gestures.R.drawable.naval_ensign_of_india
import com.github.nisrulz.sensey.TouchTypeDetector




class MainActivity : Activity() {
    var cLogType = "Gestures_MJ> "
//    var mImageView = null
    private lateinit var mGestureDetector: GestureDetector

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layout.activity_main)
        val name = object{}.javaClass.enclosingMethod.name
        Toast.makeText(this@MainActivity, name, Toast.LENGTH_LONG).show()
        val mImageView: ImageView = findViewById(R.id.imageView)
        var touchListener = View.OnTouchListener{
                v,event->
            onTouchEvent(event)
        }
        Sensey.getInstance().init(this);
        val touchTypListener = object : TouchTypeDetector.TouchTypListener {
            override fun onTwoFingerSingleTap() {
                // Two fingers single tap
            }

            override fun onThreeFingerSingleTap() {
                // Three fingers single tap
            }

            override fun onDoubleTap() {
                Log.d("GESTURETAGBUTTON",">>>> double  tap ")
                // Double tap
            }

            override fun onScroll(scrollDirection: Int) {
                when (scrollDirection) {
                    TouchTypeDetector.SCROLL_DIR_UP -> {
                    }
                    TouchTypeDetector.SCROLL_DIR_DOWN -> {
                    }
                    TouchTypeDetector.SCROLL_DIR_LEFT -> {
                    }
                    TouchTypeDetector.SCROLL_DIR_RIGHT -> {
                    }
                    else -> {
                    }
                }// Scrolling Up
                // Scrolling Down
                // Scrolling Left
                // Scrolling Right
                // Do nothing
            }

            override fun onSingleTap() {
                // Single tap
            }

            override fun onSwipe(swipeDirection: Int) {
                when (swipeDirection) {
                    TouchTypeDetector.SWIPE_DIR_UP -> {
                    }
                    TouchTypeDetector.SWIPE_DIR_DOWN -> {
                    }
                    TouchTypeDetector.SWIPE_DIR_LEFT -> {
                    }
                    TouchTypeDetector.SWIPE_DIR_RIGHT -> {
                    }
                    else -> {
                    }
                }// Swipe Up
                // Swipe Down
                // Swipe Left
                // Swipe Right
                //do nothing
            }

            override fun onLongPress() {
                // Long press
            }
        }
//
    }
}
