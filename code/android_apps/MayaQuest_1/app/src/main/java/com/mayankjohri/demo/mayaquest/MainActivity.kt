package com.mayankjohri.demo.mayaquest

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.CheckBox
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*
import kotlin.collections.ArrayList


class MainActivity : AppCompatActivity() {
    //    var currentQuestionId: Int = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val controller = AppDBController(this)
        val cursor = controller.selectAll("select id from questions")
        var quests = arrayListOf<Int>()
        while (cursor!!.moveToNext()) {
            var index: Int = cursor.getColumnIndex("id")
            var id: Int = cursor.getInt(index)
            quests.add(id)
        }
        cursor.close()
        Log.d("TAG", "${quests.count()}")
        quests = shuffle(quests)
        var currentIndex: Int = 0

        populate(quests[currentIndex])

        bt_start.setOnClickListener {
            currentIndex = 0
            quests = shuffle(quests)
            bt_evaluate.isEnabled = true
            populate(quests[currentIndex])

        }

        bt_evaluate.setOnClickListener {
            var (result, ans) = evaluate(quests[currentIndex])
            if (result) {
                Log.d("TAG", "Correct")
                Snackbar.make(ll_choices, "Correct Answer", Snackbar.LENGTH_SHORT).show()
            } else {
                Log.d("TAG", "Wrong")
                Snackbar.make(ll_choices, "Wrong, Correct Answer: $ans", Snackbar.LENGTH_SHORT).show()
            }
//            Thread.sleep(2_000)
            if (currentIndex + 1 < quests.count()) {
                currentIndex += 1
                populate(quests[currentIndex])
            } else {
                Log.d("TAG", "All questions Reviewed")
                bt_evaluate.isEnabled = false
            }
        }
    }

    private fun evaluate(id: Int): Pair<Boolean, ArrayList<String>> {
        var choices = arrayListOf<Int>()

        var answers = arrayListOf<String>()
        val controller = AppDBController(this)
        val cursor = controller.selectAll("select id, choice from choices where answer=1 and q_id=$id")
        while (cursor!!.moveToNext()) {
            choices.add(cursor.getInt(cursor.getColumnIndex("id")))
            answers.add(cursor.getString(cursor.getColumnIndex("choice")))
        }
        cursor.close()
        var selected = arrayListOf<Int>()
        for (i in 0..(ll_choices.childCount - 1)) {
            var cbview: CheckBox = ll_choices.getChildAt(i) as CheckBox
            if (cbview.isChecked) {
                selected.add(cbview.id)
            }
        }

        if ((choices.containsAll(selected)) && (choices.count() == selected.count())) return Pair(true, answers)

        return Pair(false, answers)
    }

    private fun populate(currentQuestionId: Int) {
        var currentQuest = getQuestion(currentQuestionId)
        var currentChoices = getChoices(currentQuestionId)
        tv_Questions.text = currentQuest

        ll_choices.removeAllViews()
        var x = 0
        currentChoices?.forEach {
            var cb = CheckBox(this)
            cb.text = it.choice
            cb.id = it.idx!!
            Log.d("TAG", "cb.id = ${cb.id}")
            ll_choices.addView(cb)
            x += 1
        }
    }

    private fun getQuestion(id: Int): String {
        var question = ""
        val controller = AppDBController(this)
        val cursor = controller.selectAll("select question from questions where id=$id")

        Log.d("TAG 1", "Data")
        while (cursor!!.moveToNext()) {
            Log.d("TAG 1", "Data")
            question = cursor.getString(cursor.getColumnIndex("question"))
            Log.d("TAG 2", question)
        }

        cursor.close()
        return question
    }

    private fun shuffle(lst: ArrayList<Int>): ArrayList<Int>{
        val seed = System.nanoTime()
        lst.shuffle(Random(seed))
        return lst
    }

    private fun getChoices(id: Int): ArrayList<Choices>? {
        var choices = arrayListOf<Choices>()
        val controller = AppDBController(this)
        val cursor = controller.selectAll("select id, choice, choice_type, answer from choices where q_id=$id")

        Log.d("TAG 1", "Data")
        while (cursor!!.moveToNext()) {
            Log.d("TAG 1", "Data")
            var id = cursor.getInt(cursor.getColumnIndex("id"))
            var choice = cursor.getString(cursor.getColumnIndex("choice"))
            var choiceType = cursor.getInt(cursor.getColumnIndex("choice_type"))
            var answer = cursor.getInt(cursor.getColumnIndex("answer"))
            Log.d("TAG 2", choice)
            choices.add(Choices(id, choice, choiceType, answer))
        }
        cursor.close()
        return choices
    }
}

