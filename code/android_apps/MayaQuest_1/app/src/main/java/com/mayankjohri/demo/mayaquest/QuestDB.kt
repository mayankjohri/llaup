package com.mayankjohri.demo.mayaquest

import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import com.abnerescocio.assetssqlite.lib.AssetsSQLite

//import android.content.ContentValues
//import android.content.Context
//import android.content.SharedPreferences
//import android.database.sqlite.SQLiteDatabase
//import android.database.sqlite.SQLiteOpenHelper
//import java.io.File
//import java.io.FileOutputStream
//
//
//class QuestDB(val context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
//
//
//    private val preferences: SharedPreferences = context.getSharedPreferences(
//        "${context.packageName}.database_versions",
//        Context.MODE_PRIVATE
//    )
//    private fun installedDatabaseIsOutdated(): Boolean {
//        return preferences.getInt(DATABASE_NAME, 0) < DATABASE_VERSION
//    }
//
//    private fun writeDatabaseVersionInPreferences() {
//        preferences.edit().apply {
//            putInt(DATABASE_NAME, DATABASE_VERSION)
//            apply()
//        }
//    }
//
//    private fun installDatabaseFromAssets() {
//        val inputStream = context.assets.open("$ASSETS_PATH/$DATABASE_NAME.sqlite3")
//
//        try {
//            val outputFile = File(context.getDatabasePath(DATABASE_NAME).path)
//            val outputStream = FileOutputStream(outputFile)
//
//            inputStream.copyTo(outputStream)
//            inputStream.close()
//
//            outputStream.flush()
//            outputStream.close()
//        } catch (exception: Throwable) {
//            throw RuntimeException("The $DATABASE_NAME database couldn't be installed.", exception)
//        }
//    }
//
//    @Synchronized
//    private fun installOrUpdateIfNecessary() {
//        if (installedDatabaseIsOutdated()) {
//            context.deleteDatabase(DATABASE_NAME)
//            installDatabaseFromAssets()
//            writeDatabaseVersionInPreferences()
//        }
//    }
//
//    override fun getWritableDatabase(): SQLiteDatabase {
//        throw RuntimeException("The $DATABASE_NAME database is not writable.")
//    }
//
//    override fun getReadableDatabase(): SQLiteDatabase {
//        installOrUpdateIfNecessary()
//        return super.getReadableDatabase()
//    }
//
//    override fun onCreate(db: SQLiteDatabase?) {
//        // Nothing to do
//    }
//
//    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
//        // Nothing to do
//    }
//
//
//    companion object {
//        const val DATABASE_VERSION = 1
//        const val ASSETS_PATH = "db"
//        const val DATABASE_NAME = "mayaQuest_gcp"
//        const val TABLE_QUESTIONS = "questions"
//        //        val COLUMN_ID = "id"
//        const val COLUMN_QUESTIONS="question"
//    }
//
////    fun addQuestions(questions: Questions) {
////        val values = ContentValues()
////        values.put(COLUMN_QUESTIONS, questions.questions)
////
////        val db = this.writableDatabase
////
////        db.insert(TABLE_QUESTIONS, null, values)
////        db.close()
////    }
////
////    fun findQuestions(): ArrayList<Questions>? {
////
////        val query =
////            "SELECT * FROM $TABLE_QUESTIONS"
////        val questionList: ArrayList<Questions> = arrayListOf()
////        val db = this.writableDatabase
////        val cursor = db.rawQuery(query, null)
////        var question: Questions? = null
////        if (cursor.moveToFirst()) {
////            cursor.moveToFirst()
////
////            val id = Integer.parseInt(cursor.getString(0))
////            val name = cursor.getString(1)
////            questionList.add(Questions(id, name))
////            cursor.close()
////        }
////
////        db.close()
////        return questionList
////    }
//
//
//}

class AppAssetsSQLite(context: Context): AssetsSQLite(context, DATABASE_NAME) {
    companion object {
        const val DATABASE_NAME = "mayaQuest_gcp.db"
    }
}

open class AppDBController(context: Context) {
    private val sqLiteDatabase: SQLiteDatabase?

    init {
        val appAssetsSQLite = AppAssetsSQLite(context)
        sqLiteDatabase = appAssetsSQLite.writableDatabase
    }

    fun selectAll(tableName: String): Cursor? {
        return sqLiteDatabase?.rawQuery(tableName, null)
    }

    fun close() {
        sqLiteDatabase?.close()
    }
}