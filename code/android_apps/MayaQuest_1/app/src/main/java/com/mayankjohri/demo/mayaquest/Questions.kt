package com.mayankjohri.demo.mayaquest

class Questions {
    var id: Int = 0
    var questions: String?=null

    constructor(id: Int, questions: String){
        this.id = id
        this.questions = questions
    }
}

class Choices {
    var idx: Int?=0
    var choice: String? = null
    var choice_type: Int? = 0
    var answer: Int? = 0

    constructor(idx: Int?=0, choice: String, choice_type: Int? = 0, answer: Int?=0){
//        this.q_id = q_id
        this.idx = idx
        this.choice = choice
        this.choice_type = choice_type
        this.answer = answer
    }
}