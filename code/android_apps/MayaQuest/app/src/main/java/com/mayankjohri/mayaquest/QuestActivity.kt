package com.mayankjohri.mayaquest

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.CheckBox
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import kotlin.random.Random


class QuestActivity : AppCompatActivity() {
    private lateinit var questDB: String
    private var quests = arrayListOf<Int>()
    private var currentIndex: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        questDB = intent.getStringExtra("QUEST_DB")
        val controller = AppDBController(this, questDB)
        val cursor = controller.selectAll("select id from questions")
        while (cursor!!.moveToNext()) {
            val index: Int = cursor.getColumnIndex("id")
            val id: Int = cursor.getInt(index)
            quests.add(id)
        }
        cursor.close()
        Log.d("TAG", "${quests.count()}")
        quests = shuffle(quests)
        populate(quests[currentIndex])


        bt_evaluate.setOnClickListener {
            bt_evaluate.isEnabled = false
            Log.d("TAG", "isEnabled: ${bt_evaluate.isEnabled}")
            val (result, ans) = evaluate(quests[currentIndex])
            if (result) {
                Log.d("TAG", "Correct")
                Snackbar.make(ll_choices, "Correct Answer", Snackbar.LENGTH_LONG).show()
            } else {
                Log.d("TAG", "Wrong")
                Snackbar.make(ll_choices, "Wrong, Correct Answer: $ans", Snackbar.LENGTH_LONG).show()
            }
            Thread(Runnable {
                try {
                    Thread.sleep(4000)
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                }

                runOnUiThread {
                    if (currentIndex + 1 < quests.count()) {
                        currentIndex += 1
                        populate(quests[currentIndex])
                    } else {
                        Snackbar.make(ll_choices, "@string:completed_test", Snackbar.LENGTH_LONG).show()
                        Log.d("TAG", "@string:completed_test")
                        bt_evaluate.isEnabled = false
                    }
                    bt_evaluate.isEnabled = true
                }
            }).start()


        }
    }

    private fun evaluate(id: Int): Pair<Boolean, ArrayList<String>> {
        val choices = arrayListOf<Int>()
        val answers = arrayListOf<String>()
        val controller = AppDBController(this, this.questDB)
        val cursor = controller.selectAll("select id, choice from choices where answer=1 and q_id=$id")
        while (cursor!!.moveToNext()) {
            choices.add(cursor.getInt(cursor.getColumnIndex("id")))
            answers.add(cursor.getString(cursor.getColumnIndex("choice")))
        }
        cursor.close()
        val selected = arrayListOf<Int>()
        for (i in 0..(ll_choices.childCount - 1)) {
            val cbview: CheckBox = ll_choices.getChildAt(i) as CheckBox
            if (cbview.isChecked) {
                selected.add(cbview.id)
            }
        }

        if ((choices.containsAll(selected)) && (choices.count() == selected.count())) return Pair(true, answers)

        return Pair(false, answers)
    }

    private fun populate(currentQuestionId: Int) {
        val currentQuest = getQuestion(currentQuestionId)
        val currentChoices = getChoices(currentQuestionId)
        tv_Questions.text = currentQuest

        ll_choices.removeAllViews()
        var x = 0
        currentChoices?.forEach {
            val cb = CheckBox(this)
            cb.text = it.choice
            cb.id = it.idx!!
            Log.d("TAG", "cb.id = ${cb.id}")
            ll_choices.addView(cb)
            x += 1
        }
    }

    private fun getQuestion(id: Int): String {
        var question = ""
        val controller = AppDBController(this, questDB)
        val cursor = controller.selectAll("select question from questions where id=$id")

        Log.d("TAG 1", "Data")
        while (cursor!!.moveToNext()) {
            Log.d("TAG 1", "Data")
            question = cursor.getString(cursor.getColumnIndex("question"))
            Log.d("TAG 2", question)
        }

        cursor.close()
        return question
    }

    private fun shuffle(lst: ArrayList<Int>): ArrayList<Int> {
        val seed = System.nanoTime()
        lst.shuffle(Random(seed))
        return lst
    }

    private fun getChoices(id: Int): ArrayList<Choices>? {
        val choices = arrayListOf<Choices>()
        val controller = AppDBController(this, questDB)
        val cursor = controller.selectAll("select id, choice, choice_type, answer from choices where q_id=$id")

        Log.d("TAG 1", "Data")
        while (cursor!!.moveToNext()) {
            Log.d("TAG 1", "Data")
            val ids = cursor.getInt(cursor.getColumnIndex("id"))
            val choice = cursor.getString(cursor.getColumnIndex("choice"))
            val choiceType = cursor.getInt(cursor.getColumnIndex("choice_type"))
            val answer = cursor.getInt(cursor.getColumnIndex("answer"))
            Log.d("TAG 2", choice)
            choices.add(Choices(ids, choice, choiceType, answer))
        }
        cursor.close()
        return choices
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId
        if (id == R.id.action_restart) {
            restartQuest()
        } else if (id == R.id.action_select_quest) {
            val intent = Intent(baseContext, TopicSelectionActivity::class.java)
            startActivity(intent)
        } else if (id == R.id.action_exit_out) {
            finishAffinity()
        }
        return when (item.itemId) {
            R.id.action_select_quest -> true
            R.id.action_restart -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun restartQuest() {
        Log.d("TAG", "inside restartQuest $quests.count()")
        currentIndex = 0
        Log.d("TAG", "quests.count(): $quests.count()")
        quests = shuffle(quests)
        bt_evaluate.isEnabled = true
        if(quests.count() > 0){
            populate(quests[currentIndex])
        }
    }


}
