package com.mayankjohri.mayaquest

import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import com.abnerescocio.assetssqlite.lib.AssetsSQLite

class AppAssetsSQLite(context: Context, DATABASE_NAME: String): AssetsSQLite(context, DATABASE_NAME)

open class AppDBController(context: Context, DATABASE_NAME: String) {
    private val sqLiteDatabase: SQLiteDatabase?

    init {
        val appAssetsSQLite = AppAssetsSQLite(context, DATABASE_NAME)
        sqLiteDatabase = appAssetsSQLite.writableDatabase
    }

    fun selectAll(tableName: String): Cursor? {
        return sqLiteDatabase?.rawQuery(tableName, null)
    }

    fun close() {
        sqLiteDatabase?.close()
    }
}
