package com.mayankjohri.mayaquest

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.activity_topic_selection.*

class TopicSelectionActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_topic_selection)
        val questsMap = LinkedHashMap<String, String>()

        questsMap["GCP Professional Cloud Architect"] = "mayaQuest_gcp.db"
        questsMap["GCP Professional Data Engineer"] = "mayaQuest_gcp_PDE.db"

//        val questsMap = hashMapOf(
//            "GCP Professional Cloud Architect" to "mayaQuest_gcp.db",
//            "GCP Professional Data Engineer" to "mayaQuest_gcp_PDE.db")
        val questList : ArrayList<String> = arrayListOf()
        questsMap.keys.forEach {
            questList.add(it)
        }


        val topics = ArrayAdapter(this,
            android.R.layout.simple_spinner_item,
            questList
        )
//        // Set layout to use when the list of choices appear
        topics.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
//        // Set Adapter to Spinner
        s_topics!!.adapter = topics

        bt_select_topic.setOnClickListener {
            Log.d("TAG", s_topics.selectedItem.toString())
            var questDB = questsMap[s_topics.selectedItem.toString()]

            val intent = Intent(baseContext, QuestActivity::class.java)
            Log.d("TAG", "questDB $questDB")
            intent.putExtra("QUEST_DB", questDB)
            startActivity(intent)
        }
    }

}
