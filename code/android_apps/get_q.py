# coding=utf8
"""."""
# the above tag defines encoding for this document and
# is for Python 2.x compatibility
import re
import sqlite3
from sqlite3 import Error


def create_connection(db_file):
    """ create a database connection to the SQLite database
        specified by db_file
    :param db_file: database file
    :return: Connection object or None
    """
    try:
        conn = sqlite3.connect(db_file)
        return conn
    except Error as error:
        print(error)

    return None


def insert_data(conn, query, data):
    cur = conn.cursor()
    cur.execute(query, data)
    return cur.lastrowid


def insert_question(conn, question):
    sql = "insert into questions (question) values (?) "
    return insert_data(conn, sql, (question,))


def insert_choices(conn, qid, cho, answer, choice_type=0):
    """."""
    sql = """Insert into choices (q_id, choice, choice_type, answer)
                Values (?,?,?,?)"""
    return insert_data(conn, sql, (qid, cho, choice_type, answer))


regex = r"QUESTION (.*?)Section: "

with open("q1.txt") as f:
    test_str = f.read()
    matches = re.finditer(regex, test_str, re.DOTALL)

for matchNum, match in enumerate(matches, start=1):
    question = "\n".join(match.group().strip().split('\n')[1:])
    choices = question.split('\nA.')
    question = choices[0].strip()
    # choices = choices.split('\n')
    ans = choices[1].split("Correct Answer: ")[1].split("Section:")[0].strip()

    regex = "[A-Z][.] "
    choices = '\n'.join(choices[1:][0].split('\n')[:-1])
    choices = re.split(regex, choices)

    print(">>> Questions:", question)
    print("##### Choices", choices)
    print(">>Ans", ans)
    try:
        db = "mayaQuest_gcp.db"
        con = create_connection(db)
        with con:
            question_id = insert_question(con, question)
            x = 65
            for choice in choices:
                if choice.strip():
                    print(con, question_id, choice, chr(x) in ans)
                    insert_choices(con, question_id, choice,
                                   1 if chr(x) in ans else 0)
                    x +=1
    except Exception as e:
        pass
