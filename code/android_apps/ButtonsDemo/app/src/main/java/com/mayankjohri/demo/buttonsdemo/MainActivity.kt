package com.mayankjohri.demo.buttonsdemo

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.GestureDetector
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast

import MyGestureDetector

private const val DEBUG_TAG = "Gestures"


class MainActivity : AppCompatActivity() {
    lateinit var mygestureDetector: GestureDetector

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
//         val txt_view = findViewById<TextView>(R.id.textview)
        val textView: TextView = findViewById(R.id.textview)
        val button = findViewById<Button>(R.id.button_ok)

        mygestureDetector = GestureDetector(this@MainActivity, MyGestureDetector())
        var touchListener = View.OnTouchListener{
                v,event->
            mygestureDetector.onTouchEvent(event)
        }
        textView.setOnTouchListener(touchListener)

        button.setOnClickListener(fun(_: View) {
//            var results: String
            textView.text = "Button Clicked ok"
        })
//        textView.setOnClickListener(fun(_: View) {
////            var results: String
//            textView.text = "Someone clicked me"
//            Toast.makeText(this@MainActivity, "Its setOnClickListener toast!", Toast.LENGTH_LONG).show()
//        })


    }

}