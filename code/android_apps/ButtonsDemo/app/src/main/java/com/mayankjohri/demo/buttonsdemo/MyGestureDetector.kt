import android.util.Log
import android.view.GestureDetector
import android.view.MotionEvent

class MyGestureDetector : GestureDetector.SimpleOnGestureListener() {
    var GESTURETAGBUTTON = "MAINACTIVITYTOUCHMEBUTTON"
    override fun onSingleTapUp(e: MotionEvent?): Boolean {
        Log.d(GESTURETAGBUTTON,"single tap up")
        return super.onSingleTapUp(e)
    }

    override fun onDown(e: MotionEvent?): Boolean {
        Log.d(GESTURETAGBUTTON,"on down")

        return super.onDown(e)
    }


    override fun onDoubleTap(e: MotionEvent?): Boolean {
        Log.d(GESTURETAGBUTTON,">>>> double  tap ")
        return super.onDoubleTap(e)
    }


    override fun onContextClick(e: MotionEvent?): Boolean {
        return super.onContextClick(e)
    }

    override fun onSingleTapConfirmed(e: MotionEvent?): Boolean {
        Log.d(GESTURETAGBUTTON,"single confirmed")
        return super.onSingleTapConfirmed(e)
    }

    override fun onShowPress(e: MotionEvent?) {
        super.onShowPress(e)
        Log.d(GESTURETAGBUTTON,"on show press")
    }

    override fun onDoubleTapEvent(e: MotionEvent?): Boolean {
        Log.d(GESTURETAGBUTTON,"on double tap")
        return super.onDoubleTapEvent(e)
    }

    override fun onLongPress(e: MotionEvent?) {
        super.onLongPress(e)
        Log.d(GESTURETAGBUTTON,"on long press")

    }
}