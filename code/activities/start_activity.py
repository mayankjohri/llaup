# -*- coding: utf-8 -*-
"""
Tests Various Text views of the Sample Application

"""

from time import sleep
from appium import webdriver
from appium.webdriver.common.touch_action import TouchAction
from appium.webdriver.common.multi_action import MultiAction

def set_desired_caps():
    """."""
    desired_caps = {}
    desired_caps['platformName'] = 'Android'
    desired_caps['platformVersion'] = '9'
    desired_caps['deviceName'] = 'emulator-5554'
    desired_caps['appPackage'] = 'com.mayankjohri.demo.dropdownmenus'
    desired_caps['appActivity'] = '.MainActivity'
    return desired_caps


def main():
    """."""
    try:
        driver = webdriver.Remote('http://localhost:4723/wd/hub',
                                  set_desired_caps())
        sleep(3)
        driver.start_activity(app_package='com.mayankjohri.demo.dropdownmenus',
                              app_activity=".LoginActivity")

        sleep(3)
    except Exception as error:
        print(error)
    finally:
        if 'driver' in locals():
            driver.quit()


main()
