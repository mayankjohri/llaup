# Open navigation drawer

"""
Tests Various Text views of the Sample Application

"""
from time import sleep
from appium import webdriver
from appium.webdriver.common.touch_action import TouchAction

def set_desired_caps():
    """."""
    desired_caps = {}
    desired_caps['platformName'] = 'Android'
    desired_caps['platformVersion'] = '9'
    desired_caps['deviceName'] = 'emulator-5554'
    desired_caps['appPackage'] = 'com.arya.mayankjohri.satyarthprakash'
    desired_caps['appActivity'] = '.MainActivity'
    return desired_caps


def main():
    """."""
    try:
        driver = webdriver.Remote('http://localhost:4723/wd/hub',
                                  set_desired_caps())

        e_menu_home_desc_sel = "Open navigation drawer"
        e_menu_preface_sel = "com.arya.mayankjohri.satyarthprakash:id/" \
            "design_menu_item_text"
        e_web_view_sel = "com.arya.mayankjohri.satyarthprakash:id/web_view"
        e_menu_home = driver.find_element_by_android_uiautomator(
            'new UiSelector().descriptionContains("{}")'
            '.instance(0);'.format(e_menu_home_desc_sel))
        e_menu_home.click()
        sleep(2)
        driver.find_element_by_id(e_menu_preface_sel).click()
        sleep(2)
        e_web_view = driver.find_element_by_id(e_web_view_sel)
        driver.switch_to.context("Webview1")
        driver.switch_to.context("NATIVE_APP")


    except Exception as e:
        print("error")
        print(e)
    finally:
        if 'driver' in locals():
            driver.quit()
        print("Bye Bye")


main()
