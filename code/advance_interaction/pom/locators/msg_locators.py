"""."""
from appium.webdriver.common.mobileby  import MobileBy as MBy

NAME = "com.google.android.apps.messaging"

NEW_MSG = By.ID, NAME + ":id/start_new_conversation_button"
ET_PHONE_NUMBER = By.ID, NAME + ":id/recipient_text_view"
COMPOSE_MSG = By.ID, NAME + ":id/compose_message_text"
SEND_SMS_BUTTON = By.ID, NAME + ":id/send_message_button_container"
