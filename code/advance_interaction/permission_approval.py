# -*- coding: utf-8 -*-
"""
Tests Various Text views of the Sample Application

"""

from time import sleep
from appium import webdriver


def set_desired_caps():
    """."""
    desired_caps = {}
    desired_caps['platformName'] = 'Android'
    desired_caps['platformVersion'] = '9'
    desired_caps['deviceName'] = 'emulator-5554'
    desired_caps['appPackage'] = 'com.mayankjohri.demo.dropdownmenus'
    desired_caps['appActivity'] = '.LoginActivity'
    # autoAcceptAlerts works only for iOS
    desired_caps["autoAcceptAlerts"] = True
    return desired_caps


def approve_permission(driver):
    try:
        while True:
            ele = driver.find_element_by_id("com.android.packageinstaller:id/"
                                            "permission_allow_button")
            ele.click()
    except:
        pass


def main():
    """."""
    try:
        desired_caps = set_desired_caps()
        driver = webdriver.Remote('http://localhost:4723/wd/hub',
                                  desired_caps)
        approve_permission(driver)
        driver.start_activity(desired_caps['appPackage'],
                              ".MainActivity")
        sleep(5)
    except Exception as error:
        print(error)
    finally:
        if 'driver' in locals():
            driver.quit()


main()
