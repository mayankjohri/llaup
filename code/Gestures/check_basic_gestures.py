"""
Tests Various Text views of the Sample Application

"""
from time import sleep
from appium import webdriver
from appium.webdriver.common.touch_action import TouchAction

def set_desired_caps():
    """."""
    desired_caps = {}
    desired_caps['platformName'] = 'Android'
    desired_caps['platformVersion'] = '9'
    desired_caps['deviceName'] = 'emulator-5554'
    desired_caps['appPackage'] = 'wycliffe.com.iknowyourtouch'
    desired_caps['appActivity'] = '.MainActivity'
    return desired_caps


def main():

    try:
        driver = webdriver.Remote('http://localhost:4723/wd/hub',
                                  set_desired_caps())

        ele_name = "android.widget.RelativeLayout"
        print("Lets get the textbox {ele_name}".format(ele_name=ele_name))

        element = driver.find_element_by_class_name(ele_name)
        actions = TouchAction(driver)
        actions.tap(element)
        actions.perform()
        sleep(5)

        actions.tap(element).wait(5).long_press(element).perform()
    except Exception as e:
        print(e)
    finally:
        if 'driver' in locals():
            driver.quit()
        print("Bye Bye")


main()
