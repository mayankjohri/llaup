"""
Tests Various Text views of the Sample Application

"""
from time import sleep
from appium import webdriver
from appium.webdriver.common.touch_action import TouchAction
from appium.webdriver.common.multi_action import MultiAction

def set_desired_caps():
    """."""
    desired_caps = {}
    desired_caps['platformName'] = 'Android'
    desired_caps['platformVersion'] = '9'
    desired_caps['deviceName'] = 'emulator-5554'
    desired_caps['appPackage'] = 'com.mayankjohri.demo.multitouch'
    desired_caps['appActivity'] = '.MainActivity'
    return desired_caps


def main():

    try:
        driver = webdriver.Remote('http://localhost:4723/wd/hub',
                                  set_desired_caps())

        ele_name = "com.mayankjohri.demo.multitouch:id/imageView"
        print("Lets get the textbox {ele_name}".format(ele_name=ele_name))

        element = driver.find_element_by_id(ele_name)
        # print(help(TouchAction))
        # print(dir(TouchAction))
        a1 = TouchAction()
        a1.press(x=500, y=1200)
        a1.move_to(x=500, y=200)
        a1.release()

        a2 = TouchAction()
        a2.press(x=500, y=500)
        a2.move_to(x=500, y=1900)
        a2.release()

        ma = MultiAction(driver)
        ma.add(a1, a2)
        ma.perform()
        sleep(5)

        # actions.tap(element).wait(5).double_tap(element).perform()
    except Exception as e:
        print(e)
    finally:
        if 'driver' in locals():
            driver.quit()
        print("Bye Bye")


main()
