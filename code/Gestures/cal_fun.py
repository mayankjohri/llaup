"""
Tests Various Text views of the Sample Application

"""
from time import sleep
from appium import webdriver
from appium.webdriver.common.touch_action import TouchAction
from appium.webdriver.common.multi_action import MultiAction

def set_desired_caps():
    """."""
    desired_caps = {}
    desired_caps['platformName'] = 'Android'
    desired_caps['platformVersion'] = '9'
    desired_caps['deviceName'] = 'emulator-5554'
    desired_caps['appPackage'] = 'com.android.calculator2'
    desired_caps['appActivity'] = '.Calculator'
    return desired_caps


def main():

    try:
        driver = webdriver.Remote('http://localhost:4723/wd/hub',
                                  set_desired_caps())

        a1 = TouchAction(driver)
        element=driver.find_element_by_id("com.android.calculator2:id/digit_9")
        a1.tap(element)

        # element=driver.find_element_by_id("com.android.calculator2:id/digit_7")
        # a1.tap(element)

        # element=driver.find_element_by_id("com.android.calculator2:id/digit_6")
        # a1.tap(element)


        a2 = TouchAction(driver)

        element=driver.find_element_by_id("com.android.calculator2:id/digit_1")
        a1.tap(element)

        # element=driver.find_element_by_id("com.android.calculator2:id/digit_2")
        # a1.tap(element)

        # element=driver.find_element_by_id("com.android.calculator2:id/digit_3")
        # a1.tap(element)
        ma = MultiAction(driver)
        ma.add(a1, a2)
        ma.perform()
        sleep(5)

        # actions.tap(element).wait(5).double_tap(element).perform()
    except Exception as e:
        print(e)
    finally:
        if 'driver' in locals():
            driver.quit()
        print("Bye Bye")


main()
