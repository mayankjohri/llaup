"""
Tests Various Text views of the Sample Application

"""
from time import sleep
from appium import webdriver
from appium.webdriver.common.touch_action import TouchAction

def set_desired_caps():
    """."""
    desired_caps = {}
    desired_caps['platformName'] = 'Android'
    desired_caps['platformVersion'] = '9'
    desired_caps['deviceName'] = 'emulator-5554'
    desired_caps['appPackage'] = 'com.mayankjohri.demo.mayaappiumtextboxesdemo'
    desired_caps['appActivity'] = '.MainActivity'
    return desired_caps


def main():

    try:
        driver = webdriver.Remote('http://localhost:4723/wd/hub',
                                  set_desired_caps())

        ele_name = "com.mayankjohri.demo.mayaappiumtextboxesdemo:id/et_Name"
        print("Lets get the textbox {ele_name}".format(ele_name=ele_name))
        element = driver.find_element_by_id(ele_name)
        actions = TouchAction(driver)

        d_action = actions.long_press(x=338, y=341)
        d_action.perform()
        sleep(10)

    except Exception as e:
        print("error")
        print(e)
    finally:
        if 'driver' in locals():
            driver.quit()
        print("Bye Bye")


main()
