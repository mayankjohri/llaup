"""
Tests Various Text views of the Sample Application

"""
import os
from time import sleep
from appium import webdriver
# from appium.webdriver.common.mobileby  import MobileBy as MBy


def set_desired_caps():
    """
    Setting desired capabilities to be used for initiating the Appium driver. You will need to provide many options
    """
    desired_caps = {}
    desired_caps['device'] = 'Android'
    desired_caps['platformName'] = 'Android'
    desired_caps['automationName'] = 'UiAutomator2'
    desired_caps['appPackage'] = 'mj.aryaquest.in'
    desired_caps['appActivity'] = '.ui.MainActivity'
    desired_caps['noReset'] = 'true'
    return desired_caps


def main():
    """."""
    try:
        driver = webdriver.Remote('http://localhost:4723/wd/hub',
                                  set_desired_caps())

        print("Sleeping...")
        sleep(15)
        print("Waking now...")
        # driver.start_activity("com.mfine", "MainActivity")
        ele_name = "android.widget.TextView"
        print("Lets get the textbox {ele_name}".format(ele_name=ele_name))

        elements = driver.find_elements_by_class_name(ele_name)
        for ele in elements:
            print(dir(ele))

    except Exception as e:
        print(e)
    finally:
        if 'driver' in locals():
            driver.quit()
        print("Bye Bye")


main()
