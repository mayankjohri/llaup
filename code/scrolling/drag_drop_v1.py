# -*- coding: utf-8 -*-
"""
Tests Various Text views of the Sample Application

"""

from time import sleep
from appium import webdriver
from appium.webdriver.common.touch_action import TouchAction
from appium.webdriver.common.multi_action import MultiAction

def set_desired_caps():
    """."""
    desired_caps = {}
    desired_caps['platformName'] = 'Android'
    desired_caps['platformVersion'] = '9'
    desired_caps['deviceName'] = 'emulator-5554'
    desired_caps['appPackage'] = 'com.mayankjohri.demo.dragdrop'
    desired_caps['appActivity'] = '.MainActivity'
    return desired_caps


def main():
    """."""
    ids = {
        "iv_one":
            "com.mayankjohri.demo.dragdrop:id/iv_one",
        "iv_two":
            "com.mayankjohri.demo.dragdrop:id/iv_two",
        "iv_three":
            "com.mayankjohri.demo.dragdrop:id/iv_three",
        "iv_four":
            "com.mayankjohri.demo.dragdrop:id/iv_four"
    }

    try:
        driver = webdriver.Remote('http://localhost:4723/wd/hub',
                                  set_desired_caps())

        e1 = driver.find_element_by_id(ids["iv_one"])
        e2 = driver.find_element_by_id(ids["iv_two"])
        e3 = driver.find_element_by_id(ids["iv_three"])
        e4 = driver.find_element_by_id(ids["iv_four"])

        a1 = TouchAction().tap(e1).move_to(e2).release()

        a2 = TouchAction().tap(e3).move_to(e4).release()

        ma = MultiAction(driver)
        ma.add(a2, a1)
        ma.perform()

        sleep(5)
    except Exception as error:
        print(error)
    finally:
        if 'driver' in locals():
            driver.quit()


main()
