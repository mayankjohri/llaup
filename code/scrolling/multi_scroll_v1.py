# -*- coding: utf-8 -*-
"""
Tests Various Text views of the Sample Application

"""

from time import sleep
from appium import webdriver
from appium.webdriver.common.touch_action import TouchAction
from appium.webdriver.common.multi_action import MultiAction

def set_desired_caps():
    """."""
    desired_caps = {}
    desired_caps['platformName'] = 'Android'
    desired_caps['platformVersion'] = '9'
    desired_caps['deviceName'] = 'emulator-5554'
    desired_caps['appPackage'] = 'com.mayankjohri.demo.multitouchexample'
    desired_caps['appActivity'] = '.MainActivity'
    return desired_caps


def main():
    """."""
    ids = {
        "et_right":
            "com.mayankjohri.demo.multitouchexample:id/et_right",
        "et_left":
            "com.mayankjohri.demo.multitouchexample:id/et_left"
    }

    try:
        driver = webdriver.Remote('http://localhost:4723/wd/hub',
                                  set_desired_caps())
        a1 = TouchAction().press(x=200, y=275).move_to(x=200, y=1200).release()

        a2 = TouchAction(driver)
        a2.press(x=800, y=275)
        a2.move_to(x=800, y=1200)
        a2.release().perform()
        sleep(5)
        print("lets do something else, past actions are cleared already")
        a2.long_press(x=800, y=300).perform()
        # ma = MultiAction(driver)
        # ma.add(a2, a1)
        # ma.perform()

        sleep(5)
    except Exception as error:
        print(error)
    finally:
        if 'driver' in locals():
            driver.quit()


main()
