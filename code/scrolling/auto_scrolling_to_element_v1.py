# -*- coding: utf-8 -*-
"""
Tests Various Text views of the Sample Application

"""
from time import sleep
from appium import webdriver
from selenium.webdriver.common.keys import Keys


def set_desired_caps():
    """."""
    desired_caps = {}
    desired_caps['platformName'] = 'Android'
    desired_caps['platformVersion'] = '9'
    desired_caps['deviceName'] = 'emulator-5554'
    desired_caps['appPackage'] = 'com.mayankjohri.demo.mayaappiumtextboxesdemo'
    desired_caps['appActivity'] = '.MainActivity'
    return desired_caps


def main():
    """."""
    ids = {
        "multi_line":
            "com.mayankjohri.demo.mayaappiumtextboxesdemo:id/et_multi_line",
        "et_date":
            "com.mayankjohri.demo.mayaappiumtextboxesdemo:id/et_date"
    }

    try:
        driver = webdriver.Remote('http://localhost:4723/wd/hub',
                                  set_desired_caps())

        ele_id = ids['et_date']
        print("Lets get the textbox `{}`".format(ele_id))
        element = driver.find_element_by_android_uiautomator(
            'new UiScrollable(new UiSelector().scrollable(true).instance(0))'
            '.scrollIntoView(new UiSelector().resourceId("{}")'
            '.instance(0));'.format(ele_id))
        element.click()
        element.send_keys("15/08/1947")
        print(element.text)
    except Exception as error:
        print(error)
    finally:
        if driver:
            driver.quit()


main()
