"""
Identify the elements: 

Prerequisite:

1. npm install --save opencv4nodejs

"""
from time import sleep
from appium import webdriver


def set_desired_caps():
    """."""
    desired_caps = {}
    desired_caps['platformName'] = 'Android'
    desired_caps['platformVersion'] = '9'
    desired_caps['deviceName'] = 'emulator-5554'
    desired_caps['appPackage'] = 'com.mayankjohri.demo.buttonsdemo'
    desired_caps['appActivity'] = '.MainActivity'
    return desired_caps


def main():
    try:
        driver = webdriver.Remote('http://localhost:4723/wd/hub',
                                  set_desired_caps())
        ele_name = "./softgrid_helper.png"
        print("Lets get the textbox {ele_name}".format(ele_name=ele_name))

        ele = driver.find_element_by_image(ele_name)

        # ele.set_text("Done testing")
        sleep(5)
    except Exception as e:
        print(e)
    finally:
        if 'driver' in locals():
            driver.quit()
        print("Bye Bye")


main()
