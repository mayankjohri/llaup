import unittest
from appium import webdriver
from appium.options.android import UiAutomator2Options
from appium.webdriver.common.appiumby import AppiumBy
from time import sleep

import logging 


capabilities = dict(
    platformName='Android',
    automationName='uiautomator2',
    appPackage='com.example.mayaappiumdemo',
    appActivity='.MainActivity',
)

appium_server_url = 'http://localhost:4723'

class TestAppium(unittest.TestCase):
    def setUp(self) -> None:
        self.log = logging.getLogger("appium")
        self.driver = webdriver.Remote(appium_server_url, 
                                       options=UiAutomator2Options().load_capabilities(
                                           capabilities))
        self.log.debug("welcome, starting the test")

    def tearDown(self) -> None:
        sleep(10)
        if self.driver:
            self.driver.quit()

    def test_button_byUIAutomator(self) -> None:
        el = self.driver.find_element(by=AppiumBy.ANDROID_UIAUTOMATOR, 
                                      value='UiSelector().resourceId("com.example.mayaappiumdemo:id/navigation_notifications")')
        el.click()
"""
    def test_button_by_id(self) -> None:
        el = self.driver.find_element(by=AppiumBy.ID, 
                                      value='com.example.mayaappiumdemo:id/navigation_bar_item_large_label_view')
        el.click()
        print(dir(el))

    def test_button_by_xpath(self) -> None:
        el = self.driver.find_element(by=AppiumBy.XPATH, 
                                      value='//android.widget.TextView[@resource-id="com.example.mayaappiumdemo:id/navigation_bar_item_large_label_view"]')
        el.click()

    def test_button_by_accesid(self) -> None: 
        # accessibility_id
        el = self.driver.find_element(by=AppiumBy.ACCESSIBILITY_ID, 
                                      value='Notifications')
        el.click()

"""
if __name__ == '__main__':
    logging.basicConfig(level=loglevel)
    logging.basicConfig( stream=sys.stderr )
    logging.getLogger( "appium" ).setLevel( logging.DEBUG )
    unittest.main()
