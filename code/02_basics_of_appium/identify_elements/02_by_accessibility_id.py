import unittest
from appium import webdriver
from appium.options.android import UiAutomator2Options
from appium.webdriver.common.appiumby import AppiumBy
from time import sleep

import logging 


capabilities = dict(
    platformName='Android',
    automationName='uiautomator2',
    appPackage='com.example.mayaappiumdemo',
    appActivity='.MainActivity',
)

appium_server_url = 'http://localhost:4723'

def setup():
   driver = webdriver.Remote(appium_server_url, 
                             options=UiAutomator2Options().load_capabilities(
                                           capabilities))
   return driver


def test_button_by_accesid() -> None: 
    # accessibility_id
    driver = setup()
    sleep(10)
    el = driver.find_element(by=AppiumBy.ACCESSIBILITY_ID, 
                             value='Notifications')
    print(dir(el))
    print(f"{el.is_enabled() = }")
    print(f"{el.is_selected() = }")
    print(f"{el.is_displayed() = }")
    el.click()

    with open("test.png", "wb") as fp:
        fp.write(el.screenshot_as_png)
    
    sleep(5)
    teardown(driver)

def teardown(driver):
    driver.quit()

if __name__ == '__main__':
    test_button_by_accesid()
