from time import sleep
from appium import webdriver
from appium.webdriver.common.mobileby import MobileBy as MBy


def set_desired_caps():
    """."""
    desired_caps = {}
    desired_caps['platformName'] = 'Android'
    desired_caps['platformVersion'] = '9'
    desired_caps['deviceName'] = 'emulator-5554'
    desired_caps['appPackage'] = 'com.mayankjohri.demo.buttonsdemo'
    desired_caps['appActivity'] = '.MainActivity'
    return desired_caps


def main():
    """."""
    try:
        driver = webdriver.Remote('http://localhost:4723/wd/hub',
                                  set_desired_caps())
        ele_name = "(//android.widget.CheckBox[@content-desc=\"Options\"])[3]"
        print("Lets get details of: {ele_name}".format(ele_name=ele_name))
        element = driver.find_element_by_android_viewtag(ele_name)
        print("find_element_by_android_viewtag", ele_name, element.text)

        element = driver.find_element(MBy.ANDROID_UIAUTOMATOR, ele_name)
        print("find_element by.VIEWTAG:", ele_name, element.text)

        ele_name = "//android.widget.CheckBox[@content-desc=\"Options\"]"
        element = driver.ANDROID_UIAUTOMATOR(ele_name)
        print("find_elements_by_android_viewtag", ele_name, element.text)

        elements = driver.find_elements_by_xpath(ele_name)
        print("find_elements_by_xpath", ele_name)
        for ele in elements:
            print(ele.text)
    except Exception as error:
        print(error)
    finally:
        if 'driver' in locals():
            driver.quit()
        print("Bye Bye")


main()
