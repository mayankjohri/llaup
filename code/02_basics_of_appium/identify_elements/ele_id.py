"""
Identify the element using id Various Text views of the Sample Application

"""
from time import sleep
from appium import webdriver


def set_desired_caps():
    """."""
    desired_caps = {}
    desired_caps['platformName'] = 'Android'
    # desired_caps['platformVersion'] = '9'
    # desired_caps['deviceName'] = 'emulator-5554'
    desired_caps['appPackage'] = 'com.lge.clock'
    desired_caps['appActivity'] = 'com.lge.clock'
    return desired_caps


def main():
    try:
        driver = webdriver.Remote('http://localhost:4723/wd/hub',
                                  set_desired_caps())
        ele_name = "com.lge.clock:id/alarm_sound_switch_button"
        print("Lets get the textbox {ele_name}".format(ele_name=ele_name))

        ele = driver.find_element_by_id(ele_name)

        # ele.set_text("Done testing")
        sleep(5)
    except Exception as e:
        print(e)
    finally:
        if 'driver' in locals():
            driver.quit()
        print("Bye Bye")


main()
