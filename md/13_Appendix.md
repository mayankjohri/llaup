# Appendix

## Jenkins

Jenkins is the one of the most adopted open source Continuous Integration and Continuous delivery (CI/CD) tool.

### Core Features

- **Easy Installation**
  
  Jenkins is a platform-agnostic, self-contained Java-based program, ready to run with packages for Windows, Mac OS, and Unix-like operating systems.

- **Easy Configuration**
  
  Jenkins is easily set up and configured using its web interface, featuring error checks and a built-in help function.

- **Available Plugins**
  
  There are hundreds of plugins available in the Update Center, integrating with every tool in the CI and CD toolchain.

- **Extensible**
  
  Jenkins can be extended by means of its plugin architecture, providing nearly endless possibilities for what it can do.

- **Easy Distribution**
  
  Jenkins can easily distribute work across multiple machines for faster builds, tests, and deployments across multiple platforms.

- **Free Open Source**
  
  Jenkins is an open-source resource backed by heavy community support.

### Architecture

Typical Jenkins Architecture looks something similar to as shown below.

 ![](/home/mayank/code/mj/ebooks/Appium/llaup/md/images/JenkinsArchitecture.png)

#### Jenkins Master Node

#### Jobs

A job is a collection of steps that you can use to build your source code, test your code, run a shell script, or to run an Ansible role in a remote host. There are multiple job types available to support your workflow for continuous integration & continuous delivery.

#### Job Configuration

#### Users

Jenkins has its own user database. It can be used for Jenkins’s authentication.

- Jenkins’s own user database:- Set of users maintained by Jenkins’s own database.
- LDAP Integration:- Jenkins authentication using corporate LDAP configuration.

#### Credentials

If you want to save any secret information that has to be used in the jobs, you can store it as a credential. All credentials are encrypted by Jenkins.

#### Nodes

You can configure multiple slave nodes (Linux/BSD/Windows/macOS) or clouds (docker, kubernetes) for executing Jenkins jobs. Jenkins slaves are the worker nodes for the jobs configured in Jenkins server.

##### Static Nodes

These are servers (Windows/Linux) that will be configured as static slaves. These slaves will be up and running all the time and stay connected to the Jenkins server.

##### Cloud Dynamic Nodes

Jenkins Cloud slave is a concept of having dynamic slaves. Means, whenever you trigger a job, a slave will be deployed as a VM/container on demand and gets deleted once the job is completed

##### Jenkins Master-Slave Connectivity

You can connect a Jenkins master and slave in two ways

1. **Using the SSH method:** Uses the ssh protocol to connect to the slave. The connection gets initiated from the Jenkins master. Ther should be connectivity over port 22 between master and slave.
2. **Using the JNLP method:** Uses java JNLP protocol. In this method, a java agent gets initiated from the slave with Jenkins master details. For this, the master nodes firewall should allow connectivity on specified JNLP port. Typically the port assigned will be 50000. This value is configurable.

#### Global Configurations

 you have all the configurations of installed plugins and native Jenkins global configurations. Also, you can configure global environment variables under this section.

##### Plugins

### Installing and configuring

### Jobs

### Pipeline

#### Scripted Pipeline

#### Declarative Pipeline

### CI/CD With Jenkins

#### Java Continuous Integration with Jenkins

#### Jenkins PR based builds with Github Pull Request Builder Plugin