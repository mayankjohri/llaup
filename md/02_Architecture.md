# Design & Architecture

Under the hood, Appium uses vendor-provided frameworks for automation as listed below:

- **iOS 9.3 and above**: Apple's [XCUITest](https://developer.apple.com/reference/xctest)
- **iOS 9.3 and lower**: Apple's UIAutomation (deprecated)
- **Android 4.2+**: Google's [UiAutomator/UiAutomator2](https://developer.android.com/training/testing/ui-automator)
- **Android 2.3+**: Google's [Instrumentation](http://developer.android.com/reference/android/app/Instrumentation.html) & Selendroid (deprecated)
- **Windows**: Microsoft's [WinAppDriver](http://github.com/microsoft/winappdriver)

## The Selenium JSON wire protocol

The above vendor frameworks are encapsulated by Appium using W3C webdriver API's (https://www.w3.org/TR/webdriver1/), they are also known as "Selenium WebDriver". 

They specifies a client-server protocol (also known as the [JSON Wire Protocol](https://w3c.github.io/webdriver/webdriver-spec.html)). Given this client-server architecture, a client written in any language can be used to send the appropriate HTTP requests to the server. There are already clients written in every popular programming language. This also means that you're free to use whatever test runner and test framework you want; the client libraries are simply HTTP clients and can be mixed into your code any way you please. In other words, Appium & WebDriver clients are not technically "test frameworks" -- they are "automation libraries". You can manage your test environment any way you like! 

## Appium Concepts

### Client/Server Architecture

As stated above, Appium uses WebDriver API for communication, we can have a web server and one or more clients. The Web Server listen's for the client's request and performs equivalent actions on the mobile device and returns the response of the acted action. 

### Appium session

Automation is always performed in the context of a session. Clients initiate a session with a server in ways specific to each library, but they all end up sending a `POST /session` request to the server, with a JSON object called the 'desired capabilities' object. At this point the server will start up the automation session and respond with a session ID which is used for sending further commands.

### Desired capabilities

Desired capabilities are a set of keys and values (i.e., a map or hash) sent to the Appium server to tell the server what kind of automation session we're interested in starting up. There are also various capabilities which can modify the behavior of the server during automation. For example, we might set the `platformName`capability to `iOS` to tell Appium that we want an iOS session, rather than an Android or Windows one. Or we might set the `safariAllowPopups` capability to `true` in order to ensure that, during a Safari automation session, we're allowed to use JavaScript to open up new windows.

#### Android capabilities

There are few capabilities which are only available on Android based drivers. We will cover them in details in coming chapters

#### iOS capabilities

There are few capabilities which are only available on iOS based drivers. We will cover them in details in coming chapters

### The Appium server and its client libraries

#### Appium Server

Appium is a server written in Node.js. It can be built and installed [from source](http://appium.io/docs/en/contributing-to-appium/appium-from-source/index.html) or installed directly from [NPM](https://www.npmjs.com/package/appium):

```bash
$ npm install -g appium
$ appium
```

The `beta` of Appium is available via NPM with `npm install -g appium@beta`. It is the development version so it might have breaking changes. Please uninstall `appium@beta` (`npm uninstall -g appium@beta`) before installing new versions in order to have a clean set of dependencies.

#### Appium Client

There are client libraries (in Java, Ruby, Python, PHP, JavaScript, and C#) which support Appium's extensions to the WebDriver protocol. When using Appium, you want to use these client libraries instead of your regular WebDriver client. Bellow is the list of Applium client 

| Language/Framework   | Github Repo and Installation Instructions                                   |
| -------------------- | --------------------------------------------------------------------------- |
| Python               | https://github.com/appium/python-client                                     |
| Java                 | https://github.com/appium/java-client                                       |
| JavaScript (Node.js) | https://github.com/admc/wd                                                  |
| PHP                  | https://github.com/appium/php-client                                        |
| C# (.NET)            | https://github.com/appium/appium-dotnet-driver                              |
| Objective C          | https://github.com/appium/selenium-objective-c                              |
| RobotFramework       | https://github.com/jollychang/robotframework-appiumlibrary                  |
| Ruby                 | https://github.com/appium/ruby_lib, https://github.com/appium/ruby_lib_core |

#### Appium Desktop

There is a GUI wrapper around the Appium server that can be downloaded for (windows & macOS) platform. It comes bundled with everything required to run the Appium server. It also comes with an Inspector, which enables you to check out the hierarchy of your app. This can be used in identifying the elements while automating.

## References:

- https://www.w3.org/TR/webdriver1/
- https://w3c.github.io/webdriver/webdriver-spec.html
- https://developer.apple.com/reference/xctest
- https://developer.android.com/training/testing/ui-automator
- http://selendroid.io
- http://github.com/microsoft/winappdriver
- http://developer.android.com/reference/android/app/Instrumentation.htm