# Basics of Appium

Appium has the following goals

- Make platform-specific automation capabilities available under a cross-platform, standard API
- Allow easy access to this API from any programming language
- Provide tools to enable convenient community development of Appium extensions

Appium is trying to reach the above goals by using W3C WebDriver specification for API with following differences

- Appium may lack the capability to support certain WebDriver API commands on specific platforms, resulting in some commands being unavailable. For instance, operations such as retrieving or modifying cookies are not feasible within the context of native mobile app automation.
- Appium has the ability to provide automation behaviors that exceed the WebDriver API command list, even though any such commands will still be valid.

Appium using one of the following mechanisms internaly to automate the required steps

- Android
  
  - `UiAutomator2`
  
  - `Espresso`

- iOS
  
  - `XCUITest` (default)

## Appium driver

- For  Platforms

| Driver                                                               | Installation Key | Platform(s)                    | Mode(s)             |
| -------------------------------------------------------------------- | ---------------- | ------------------------------ | ------------------- |
| [Chromium](https://github.com/appium/appium-chromium-driver)         | `chromium`       | macOS, Windows, Linux          | Web                 |
| [Espresso](https://github.com/appium/appium-espresso-driver)         | `espresso`       | Android                        | Native              |
| [Gecko](https://github.com/appium/appium-geckodriver)                | `gecko`          | macOS, Windows, Linux, Android | Web                 |
| [Mac2](https://github.com/appium/appium-mac2-driver)                 | `mac2`           | macOS                          | Native              |
| [Safari](https://github.com/appium/appium-safari-driver)             | `safari`         | macOS, iOS                     | Web                 |
| [UiAutomator2](https://github.com/appium/appium-uiautomator2-driver) | `uiautomator2`   | Android                        | Native, Hybrid, Web |
| [XCUITest](https://github.com/appium/appium-xcuitest-driver)         | `xcuitest`       | iOS                            | Native, Hybrid, Web |

And of course, you can install any other drivers you find out there by using the Appium driver CLI.

### Other drivers

These drivers are not maintained by the Appium team and can be used to target additional platforms.

| Driver                                                              | Installation Key                              | Platform(s)                      | Mode(s) | Supported By          |
| ------------------------------------------------------------------- | --------------------------------------------- | -------------------------------- | ------- | --------------------- |
| [Flutter](https://github.com/appium-userland/appium-flutter-driver) | `--source=npm appium-flutter-driver`          | iOS, Android                     | Native  | Community             |
| [LG WebOS](https://github.com/headspinio/appium-lg-webos-driver)    | `--source=npm appium-lg-webos-driver`         | LG TV                            | Web     | HeadSpin              |
| [Linux](https://github.com/fantonglang/appium-linux-driver)         | `--source=npm @stdspa/appium-linux-driver`    | Linux                            | Native  | `@fantonglang`        |
| [Roku](https://github.com/headspinio/appium-roku-driver)            | `--source=npm @headspinio/appium-roku-driver` | Roku                             | Native  | HeadSpin              |
| [Tizen](https://github.com/Samsung/appium-tizen-driver)             | `--source=npm appium-tizen-driver`            | Android                          | Native  | Community / Samsung   |
| [TizenTV](https://github.com/headspinio/appium-tizen-tv-driver)     | `--source=npm appium-tizen-tv-driver`         | Samsung TV                       | Web     | HeadSpin              |
| [Windows](https://github.com/appium/appium-windows-driver)          | `--source=npm appium-windows-driver`          | Windows                          | Native  | Community / Microsoft |
| [Youi](https://github.com/YOU-i-Labs/appium-youiengine-driver)      | `--source=npm appium-youiengine-driver`       | iOS, Android, macOS, Linux, tvOS | Native  | Community / You.i     |

## Appium Clients

- For Developr/Automation Engineers

| Client                                                                                                                                       | Language | Supported By |
| -------------------------------------------------------------------------------------------------------------------------------------------- | -------- | ------------ |
| [Appium Java client](https://github.com/appium/java-client)                                                                                  | Java     | Appium Team  |
| [Appium Python client](https://github.com/appium/python-client)                                                                              | Python   | Appium Team  |
| [Appium Ruby Core client](https://github.com/appium/ruby_lib_core) (Recommended)<br>[Appium Ruby client](https://github.com/appium/ruby_lib) | Ruby     | Appium Team  |
| [WebDriverIO](https://webdriver.io)<br>[Nightwatch](https://nightwatchjs.org/)                                                               | Node.js  | Community    |
| [Appium .NET client](https://github.com/appium/dotnet-client)                                                                                | C#       | Appium Team* |
| [RobotFramework](https://github.com/serhatbolsu/robotframework-appiumlibrary)                                                                | DSL      | Community    |

## Appium Requirements

- A macOS, Linux, or Windows operating system
- [Node.js](https://nodejs.org) version in the [SemVer](https://semver.org) range `^14.17.0 || ^16.13.0 || >=18.0.0`
- [NPM](https://npmjs.com) version >= 8 (NPM is usually bundled with Node.js, but can be upgraded
  independently)

## Appium Inspector

## Using Appium Inspector

Appium also provides a tool to gather the information regarding the elements, and its called Appium Inspector. It can be  launched by downloading the appropriate binary file https://github.com/appium/appium-inspector/releases

- **Set Up Desired Capabilities**:
  
  To use the Appium server with the Appium Inspector, you must configure the desired capabilities. Desired capabilities specify details about the mobile 
  device, the app under test, and the environment in which the test will be executed. Here are some of the key desired capabilities:
  
  - `platformName`: Defines the platform (iOS or Android) the test will run.
  - `deviceName`: Specifies the name or identifier of the device.
  - `app`: Provides the path to the application under test.
  - `automationName`: Defines the automation engine used (e.g., "UiAutomator2" for Android, "XCUITest" for iOS).

  

### Best Practices

- Keep Your Appium Inspector Updated

- Verify macOS and Appium Compatibility

- Ensure Device Compatibility

- Configure Desired Capabilities Correctly

- Test on Real Devices When Possible

## Using Google Chrome for Hybrid Apps

Google Chrome can be used to test hybrid applications.

**TODO: Will discuss later**

## Elements

Elements are building blocks of any mobile application. In order to use them, we need to first first find ways to identify them. In next section, we will investigate various techniques used in identifying the elements.

### Investigate Elements

Similar to Selenium, reliably finding the address of Elements in appium is also one of the major tasks. Thankfully, we have few utilities to help us in this regards.

## Simplest Automation Example

This example, writes and reads texts from various different types of textboxes

```python
# 01_start.py
import unittest
from appium import webdriver
from appium.options.android import UiAutomator2Options
from appium.webdriver.common.appiumby import AppiumBy

capabilities = dict(
    platformName='Android',
    automationName='uiautomator2',
    appPackage='com.android.settings',
    appActivity='.Settings',
)

appium_server_url = 'http://localhost:4723'

class TestAppium(unittest.TestCase):
    def setUp(self) -> None:
        self.driver = webdriver.Remote(appium_server_url,
                                       options=UiAutomator2Options().load_capabilities(
                                           capabilities))

    def tearDown(self) -> None:
        if self.driver:
            self.driver.quit()

    def test_find_search(self) -> None:
        el = self.driver.find_element(by=AppiumBy.XPATH,
                                      value='//android.widget.TextView[@text="Search settings"]')
        el.click()

if __name__ == '__main__':
    unittest.main()
```

### Desired Capabilities<sup>1</sup>

They are a set of key/value information, which is sent from appium client to appium server, regarding the nature of tests and nature of device on which the tests need to be executed. There are two types of desired capability.

#### Comman Capabilities

Appium support multile comman capabilities, i.e. these capabilities are present for both andorid and iOS devieces. The list of them is as follows

| Capability                     | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 | Values                                                                                                                                                                                                                                               |
| ------------------------------ | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `automationName`               | Which automation engine to use                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              | **Android**  [`Appium` (default), `Selendroid`, `UiAutomator2`, `Espresso`], **iOS** [`XCUITest` (default)]                                                                                                                                          |
| `platformName`                 | Which mobile OS platform to use                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             | `iOS`, `Android`, or `FirefoxOS`                                                                                                                                                                                                                     |
| `platformVersion`              | Which mobile OS version to use                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              | e.g., `7.1`, `4.4`                                                                                                                                                                                                                                   |
| `deviceName`                   | which mobile device or emulator to use                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      | `iPhone Simulator`,  `Android Emulator`, 'Sony', etc. On iOS, this should be one of the valid devices returned by instruments with **`instruments -s devices`.**<br/> *On Android this capability is currently ignored, though it remains required.* |
| `app`                          | The absolute local path *or* remote http URL to a `.ipa` file (IOS), `.app` folder (IOS Simulator), `.apk` file (Android) or `.apks` file (Android App Bundle), or a `.zip`  file containing one of these (for .app, the .app folder must be the  root of the zip file). Appium will attempt to install this app binary on  the appropriate device first. *Note that this capability is not required  for Android if you specify `appPackage` and `appActivity` capabilities (see below). Incompatible with `browserName`.* | `/abs/path/to/my.apk` or `http://myapp.com/app.ipa`                                                                                                                                                                                                  |
| `browserName`                  | Name of mobile web browser to automate. *Should be an empty string if automating an app instead.*                                                                                                                                                                                                                                                                                                                                                                                                                           | 'Safari' for iOS and 'Chrome', 'Chromium', or 'Browser' for Android                                                                                                                                                                                  |
| `newCommandTimeout`            | How long (in seconds) Appium will wait for a new command from the client before assuming the client quit and ending the session                                                                                                                                                                                                                                                                                                                                                                                             | e.g. `60`                                                                                                                                                                                                                                            |
| `language`                     | Language to set for iOS and Android. It is only available for simulator on iOS                                                                                                                                                                                                                                                                                                                                                                                                                                              | e.g. `fr`                                                                                                                                                                                                                                            |
| `locale`                       | Locale to set for iOS and Android. It is only available for simulator on iOS. `fr_CA` format for iOS. `CA` format (country name abbreviation) for Android                                                                                                                                                                                                                                                                                                                                                                   | e.g. `fr_CA`, `CA`                                                                                                                                                                                                                                   |
| `udid`                         | Unique device identifier of the connected physical device                                                                                                                                                                                                                                                                                                                                                                                                                                                                   | e.g. `1ae203187fc012g`                                                                                                                                                                                                                               |
| `orientation`                  | (Sim/Emu-only) start in a certain orientation                                                                                                                                                                                                                                                                                                                                                                                                                                                                               | `LANDSCAPE` or `PORTRAIT`                                                                                                                                                                                                                            |
| `autoWebview`                  | Move directly into Webview context. Default `false`                                                                                                                                                                                                                                                                                                                                                                                                                                                                         | `true`, `false`                                                                                                                                                                                                                                      |
| `noReset`                      | Don't reset app state before this session.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  | `true`, `false`                                                                                                                                                                                                                                      |
| `fullReset`                    | Perform a complete reset.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   | `true`, `false`                                                                                                                                                                                                                                      |
| `eventTimings`                 | Enable or disable the reporting of the timings for various  Appium-internal events (e.g., the start and end of each command, etc.).  Defaults to `false`. To enable, use `true`. The timings are then reported as `events` property on response to querying the current session.                                                                                                                                                                                                                                            | e.g., `true`                                                                                                                                                                                                                                         |
| `enablePerformanceLogging`     | (Web and webview only) Enable Chromedriver's (on Android) or Safari's (on iOS) performance logging (default `false`)                                                                                                                                                                                                                                                                                                                                                                                                        | `true`, `false`                                                                                                                                                                                                                                      |
| `printPageSourceOnFindFailure` | When a find operation fails, print the current page source. Defaults to `false`.                                                                                                                                                                                                                                                                                                                                                                                                                                            | e.g., `true`                                                                                                                                                                                                                                         |

#### Android Only

These Capabilities are available only on Android-based drivers.

| Capability                        | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  | Values                                                                                                                                                                                                          |
| --------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `appActivity`                     | Activity name for the Android activity you want to launch from your package. This often needs to be preceded by a `.` (e.g., `.MainActivity` instead of `MainActivity`).  By default this capability is received from the package manifest  (action: android.intent.action.MAIN , category:  android.intent.category.LAUNCHER)                                                                                                                                                               | `MainActivity`, `.Settings`                                                                                                                                                                                     |
| `appPackage`                      | Java package of the Android app you want to run. By default this  capability is received from the package manifest (@package attribute  value)                                                                                                                                                                                                                                                                                                                                               | `com.example.android.myApp`, `com.android.settings`                                                                                                                                                             |
| `appWaitActivity`                 | Activity name/names, comma separated, for the Android activity you  want to wait for. By default the value of this capability is the same as  for `appActivity`. You must set it to the very first focused application activity name in case it is different from the one which is set as `appActivity` if your capability has `appActivity` and `appPackage`.                                                                                                                               | `SplashActivity`, `SplashActivity,OtherActivity`, `*`, `*.SplashActivity`                                                                                                                                       |
| `appWaitPackage`                  | Java package of the Android app you want to wait for. By default the value of this capability is the same as for `appActivity`                                                                                                                                                                                                                                                                                                                                                               | `com.example.android.myApp`, `com.android.settings`                                                                                                                                                             |
| `appWaitDuration`                 | Timeout in milliseconds used to wait for the appWaitActivity to launch (default `20000`)                                                                                                                                                                                                                                                                                                                                                                                                     | `30000`                                                                                                                                                                                                         |
| `deviceReadyTimeout`              | Timeout in seconds while waiting for device to become ready                                                                                                                                                                                                                                                                                                                                                                                                                                  | `5`                                                                                                                                                                                                             |
| `allowTestPackages`               | Allow to install a test package which has `android:testOnly="true"` in the manifest. `false` by default                                                                                                                                                                                                                                                                                                                                                                                      | `true` or `false`                                                                                                                                                                                               |
| `androidCoverage`                 | Fully qualified instrumentation class. Passed to -w in adb shell am instrument -e coverage true -w                                                                                                                                                                                                                                                                                                                                                                                           | `com.my.Pkg/com.my.Pkg.instrumentation.MyInstrumentation`                                                                                                                                                       |
| `androidCoverageEndIntent`        | A broadcast action implemented by yourself which is used to dump  coverage into file system. Passed to -a in adb shell am broadcast -a                                                                                                                                                                                                                                                                                                                                                       | `com.example.pkg.END_EMMA`                                                                                                                                                                                      |
| `androidDeviceReadyTimeout`       | Timeout in seconds used to wait for a device to become ready after booting                                                                                                                                                                                                                                                                                                                                                                                                                   | e.g., `30`                                                                                                                                                                                                      |
| `androidInstallTimeout`           | Timeout in milliseconds used to wait for an apk to install to the device. Defaults to `90000`                                                                                                                                                                                                                                                                                                                                                                                                | e.g., `90000`                                                                                                                                                                                                   |
| `androidInstallPath`              | The name of the directory on the device in which the apk will be push before install. Defaults to `/data/local/tmp`                                                                                                                                                                                                                                                                                                                                                                          | e.g. `/sdcard/Downloads/`                                                                                                                                                                                       |
| `adbPort`                         | Port used to connect to the ADB server (default `5037`)                                                                                                                                                                                                                                                                                                                                                                                                                                      | `5037`                                                                                                                                                                                                          |
| `systemPort`                      | `systemPort` used to connect to [appium-uiautomator2-server](https://github.com/appium/appium-uiautomator2-server) or [appium-espresso-driver](https://github.com/appium/appium-espresso-driver). The default is `8200` in general and selects one port from `8200` to `8299` for *appium-uiautomator2-server*, it is `8300` from `8300` to `8399` for *appium-espresso-driver*. When you run tests in parallel, you must adjust the port to avoid conflicts.                                | e.g., `8201`                                                                                                                                                                                                    |
| `remoteAdbHost`                   | Optional remote ADB server host                                                                                                                                                                                                                                                                                                                                                                                                                                                              | e.g.: 192.168.0.101                                                                                                                                                                                             |
| `androidDeviceSocket`             | Devtools socket name. Needed only when tested app is a Chromium  embedding browser. The socket is open by the browser and Chromedriver  connects to it as a devtools client.                                                                                                                                                                                                                                                                                                                 | e.g., `chrome_devtools_remote`                                                                                                                                                                                  |
| `avd`                             | Name of avd to launch                                                                                                                                                                                                                                                                                                                                                                                                                                                                        | e.g., `api19`                                                                                                                                                                                                   |
| `avdLaunchTimeout`                | How long to wait in milliseconds for an avd to launch and connect to ADB (default `60000`)                                                                                                                                                                                                                                                                                                                                                                                                   | `300000`                                                                                                                                                                                                        |
| `avdReadyTimeout`                 | How long to wait in milliseconds for an avd to finish its boot animations (default `120000`)                                                                                                                                                                                                                                                                                                                                                                                                 | `300000`                                                                                                                                                                                                        |
| `avdArgs`                         | Additional emulator arguments used when launching an avd                                                                                                                                                                                                                                                                                                                                                                                                                                     | e.g., `-netfast`                                                                                                                                                                                                |
| `useKeystore`                     | Use a custom keystore to sign apks, default `false`                                                                                                                                                                                                                                                                                                                                                                                                                                          | `true` or `false`                                                                                                                                                                                               |
| `keystorePath`                    | Path to custom keystore, default ~/.android/debug.keystore                                                                                                                                                                                                                                                                                                                                                                                                                                   | e.g., `/path/to.keystore`                                                                                                                                                                                       |
| `keystorePassword`                | Password for custom keystore                                                                                                                                                                                                                                                                                                                                                                                                                                                                 | e.g., `foo`                                                                                                                                                                                                     |
| `keyAlias`                        | Alias for key                                                                                                                                                                                                                                                                                                                                                                                                                                                                                | e.g., `androiddebugkey`                                                                                                                                                                                         |
| `keyPassword`                     | Password for key                                                                                                                                                                                                                                                                                                                                                                                                                                                                             | e.g., `foo`                                                                                                                                                                                                     |
| `chromedriverExecutable`          | The absolute local path to webdriver executable (if Chromium  embedder provides its own webdriver, it should be used instead of  original chromedriver bundled with Appium)                                                                                                                                                                                                                                                                                                                  | `/abs/path/to/webdriver`                                                                                                                                                                                        |
| `chromedriverExecutableDir`       | The absolute path to a directory to look for Chromedriver  executables in, for automatic discovery of compatible Chromedrivers.  Ignored if `chromedriverUseSystemExecutable` is `true`                                                                                                                                                                                                                                                                                                      | `/abs/path/to/chromedriver/directory`                                                                                                                                                                           |
| `chromedriverChromeMappingFile`   | The absolute path to a file which maps Chromedriver versions to the minimum Chrome that it supports. Ignored if `chromedriverUseSystemExecutable` is `true`                                                                                                                                                                                                                                                                                                                                  | `/abs/path/to/mapping.json`                                                                                                                                                                                     |
| `chromedriverUseSystemExecutable` | If `true`, bypasses automatic Chromedriver configuration and uses the version that comes downloaded with Appium. Ignored if `chromedriverExecutable` is set. Defaults to `false`                                                                                                                                                                                                                                                                                                             | e.g., `true`                                                                                                                                                                                                    |
| `autoWebviewTimeout`              | Amount of time to wait for Webview context to become active, in ms. Defaults to `2000`                                                                                                                                                                                                                                                                                                                                                                                                       | e.g. `4`                                                                                                                                                                                                        |
| `intentAction`                    | Intent action which will be used to start activity (default `android.intent.action.MAIN`)                                                                                                                                                                                                                                                                                                                                                                                                    | e.g.`android.intent.action.MAIN`, `android.intent.action.VIEW`                                                                                                                                                  |
| `intentCategory`                  | Intent category which will be used to start activity (default `android.intent.category.LAUNCHER`)                                                                                                                                                                                                                                                                                                                                                                                            | e.g. `android.intent.category.LAUNCHER`, `android.intent.category.APP_CONTACTS`                                                                                                                                 |
| `intentFlags`                     | Flags that will be used to start activity (default `0x10200000`)                                                                                                                                                                                                                                                                                                                                                                                                                             | e.g. `0x10200000`                                                                                                                                                                                               |
| `optionalIntentArguments`         | Additional intent arguments that will be used to start activity. See [Intent arguments](http://developer.android.com/reference/android/content/Intent.html)                                                                                                                                                                                                                                                                                                                                  | e.g. `--esn <EXTRA_KEY>`, `--ez <EXTRA_KEY> <EXTRA_BOOLEAN_VALUE>`, etc.                                                                                                                                        |
| `dontStopAppOnReset`              | Doesn't stop the process of the app under test, before starting the  app using adb. If the app under test is created by another anchor app,  setting this false, allows the process of the anchor app to be still  alive, during the start of the test app using adb. In other words, with `dontStopAppOnReset` set to `true`, we will not include the `-S` flag in the `adb shell am start` call. With this capability omitted or set to `false`, we include the `-S` flag. Default `false` | `true` or `false`                                                                                                                                                                                               |
| `unicodeKeyboard`                 | Enable Unicode input, default `false`                                                                                                                                                                                                                                                                                                                                                                                                                                                        | `true` or `false`                                                                                                                                                                                               |
| `resetKeyboard`                   | Reset keyboard to its original state, after running Unicode tests with `unicodeKeyboard` capability. Ignored if used alone. Default `false`                                                                                                                                                                                                                                                                                                                                                  | `true` or `false`                                                                                                                                                                                               |
| `noSign`                          | Skip checking and signing of app with debug keys, will work only with UiAutomator and not with selendroid, default `false`                                                                                                                                                                                                                                                                                                                                                                   | `true` or `false`                                                                                                                                                                                               |
| `ignoreUnimportantViews`          | Calls the `setCompressedLayoutHierarchy()` uiautomator  function. This capability can speed up test execution, since  Accessibility commands will run faster ignoring some elements. The  ignored elements will not be findable, which is why this capability has  also been implemented as a toggle-able *setting* as well as a capability. Defaults to `false`                                                                                                                             | `true` or `false`                                                                                                                                                                                               |
| `disableAndroidWatchers`          | Disables android watchers that watch for application not responding  and application crash, this will reduce cpu usage on android  device/emulator. This capability will work only with UiAutomator and not  with selendroid, default `false`                                                                                                                                                                                                                                                | `true` or `false`                                                                                                                                                                                               |
| `chromeOptions`                   | Allows passing chromeOptions capability for ChromeDriver.                                                                                                                                                                                                                                                                                                                                                                                                                                    | `chromeOptions: {args: ['--disable-popup-blocking']}`                                                                                                                                                           |
| `recreateChromeDriverSessions`    | Kill ChromeDriver session when moving to a non-ChromeDriver webview. Defaults to `false`                                                                                                                                                                                                                                                                                                                                                                                                     | `true` or `false`                                                                                                                                                                                               |
| `nativeWebScreenshot`             | In a web context, use native (adb) method for taking a screenshot, rather than proxying to ChromeDriver. Defaults to `false`                                                                                                                                                                                                                                                                                                                                                                 | `true` or `false`                                                                                                                                                                                               |
| `androidScreenshotPath`           | The name of the directory on the device in which the screenshot will be put. Defaults to `/data/local/tmp`                                                                                                                                                                                                                                                                                                                                                                                   | e.g. `/sdcard/screenshots/`                                                                                                                                                                                     |
| `autoGrantPermissions`            | Have Appium automatically determine which permissions your app requires and grant them to the app on install. Defaults to `false`. If `noReset` is `true`, this capability doesn't work.                                                                                                                                                                                                                                                                                                     | `true` or `false`                                                                                                                                                                                               |
| `networkSpeed`                    | Set the network speed emulation. Specify the maximum network upload and download speeds. Defaults to `full`                                                                                                                                                                                                                                                                                                                                                                                  | `['full','gsm', 'edge', 'hscsd', 'gprs', 'umts', 'hsdpa', 'lte', 'evdo']` Check [-netspeed option](https://developer.android.com/studio/run/emulator-commandline.html) more info about speed emulation for avds |
| `gpsEnabled`                      | Toggle gps location provider for emulators before starting the  session. By default the emulator will have this option enabled or not  according to how it has been provisioned.                                                                                                                                                                                                                                                                                                             | `true` or `false`                                                                                                                                                                                               |
| `isHeadless`                      | Set this capability to `true` to run the Emulator headless when device display is not needed to be visible. `false` is the default value. *isHeadless* is also support for iOS, check XCUITest-specific capabilities.                                                                                                                                                                                                                                                                        | e.g., `true`                                                                                                                                                                                                    |
| `otherApps`                       | App or list of apps (as a JSON array) to install prior to running tests                                                                                                                                                                                                                                                                                                                                                                                                                      | e.g., `"/path/to/app.apk"`, `https://www.example.com/url/to/app.apk`, `["/path/to/app-a.apk", "/path/to/app-b.apk"]`                                                                                            |
| `adbExecTimeout`                  | Timeout in milliseconds used to wait for adb command execution. Defaults to `20000`                                                                                                                                                                                                                                                                                                                                                                                                          | e.g., `50000`                                                                                                                                                                                                   |
| `localeScript`                    | Sets the locale [script](https://developer.android.com/reference/java/util/Locale)                                                                                                                                                                                                                                                                                                                                                                                                           | e.g., `"Cyrl"` (Cyrillic)                                                                                                                                                                                       |
| `skipDeviceInitialization`        | Skip device initialization which includes i.a.: installation and  running of Settings app or setting of permissions. Can be used to  improve startup performance when the device was already used for  automation and it's prepared for the next automation. Defaults to `false`                                                                                                                                                                                                             | `true` or `false`                                                                                                                                                                                               |

#### UIAutomator2 Only

These Capabilities are available only on the UiAutomator2 Driver

| Capability                         | Description                                                                                                                                                                                                                            | Values            |
| ---------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------- |
| `uiautomator2ServerLaunchTimeout`  | Timeout in milliseconds used to wait for an uiAutomator2 server to launch. Defaults to `20000`                                                                                                                                         | e.g., `20000`     |
| `uiautomator2ServerInstallTimeout` | Timeout in milliseconds used to wait for an uiAutomator2 server to be installed. Defaults to `20000`                                                                                                                                   | e.g., `20000`     |
| `skipServerInstallation`           | Skip uiAutomator2 server installation and use uiAutomator2 server  from the device. Can be used to improve startup performance when an  uiAutomator2 server in proper version is already installed on the  device. Defaults to `false` | `true` or `false` |

#### Espresso Only

These Capabilities are available only on the Espresso Driver

| Capability                    | Description                                                                                | Values        |
| ----------------------------- | ------------------------------------------------------------------------------------------ | ------------- |
| `espressoServerLaunchTimeout` | Timeout in milliseconds used to wait for an espresso server to launch. Defaults to `30000` | e.g., `50000` |

### iOS Only

These Capabilities are available only on the XCUITest Driver and the deprecated UIAutomation Driver

| Capability                    | Description                                                                                                                                                                                                                                                                                      | Values                                                                         |
| ----------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | ------------------------------------------------------------------------------ |
| `calendarFormat`              | (Sim-only) Calendar format to set for the iOS Simulator                                                                                                                                                                                                                                          | e.g. `gregorian`                                                               |
| `bundleId`                    | Bundle ID of the app under test. Useful for starting an app on a  real device or for using other caps which require the bundle ID during  test startup. To run a test on a real device using the bundle ID, you  may omit the 'app' capability, but you must provide 'udid'.                     | e.g. `io.appium.TestApp`                                                       |
| `udid`                        | Unique device identifier of the connected physical device                                                                                                                                                                                                                                        | e.g. `1ae203187fc012g`                                                         |
| `launchTimeout`               | Amount of time in ms to wait for instruments before assuming it hung and failing the session                                                                                                                                                                                                     | e.g. `20000`                                                                   |
| `locationServicesEnabled`     | (Sim-only) Force location services to be either on or off. Default is to keep current sim setting.                                                                                                                                                                                               | `true` or `false`                                                              |
| `locationServicesAuthorized`  | (Sim-only) Set location services to be authorized or not authorized  for app via plist, so that location services alert doesn't pop up.  Default is to keep current sim setting. Note that if you use this  setting you MUST also use the `bundleId` capability to send in your app's bundle ID. | `true` or `false`                                                              |
| `autoAcceptAlerts`            | Accept all iOS alerts automatically if they pop up. This includes  privacy access permission alerts (e.g., location, contacts, photos).  Default is false. Does not work on `XCUITest`-based tests.                                                                                              | `true` or `false`                                                              |
| `autoDismissAlerts`           | Dismiss all iOS alerts automatically if they pop up. This includes  privacy access permission alerts (e.g., location, contacts, photos).  Default is false. Does not work on `XCUITest`-based tests.                                                                                             | `true` or `false`                                                              |
| `nativeInstrumentsLib`        | Use native intruments lib (ie disable instruments-without-delay).                                                                                                                                                                                                                                | `true` or `false`                                                              |
| `nativeWebTap`                | (Sim-only) Enable "real", non-javascript-based web taps in Safari. Default: `false`. Warning: depending on viewport size/ratio this might not accurately tap an element                                                                                                                          | `true` or `false`                                                              |
| `safariInitialUrl`            | (Sim-only) (>= 8.1) Initial safari url, default is a local welcome page                                                                                                                                                                                                                          | e.g. `https://www.github.com`                                                  |
| `safariAllowPopups`           | (Sim-only) Allow javascript to open new windows in Safari. Default keeps current sim setting                                                                                                                                                                                                     | `true` or `false`                                                              |
| `safariIgnoreFraudWarning`    | (Sim-only) Prevent Safari from showing a fraudulent website warning. Default keeps current sim setting.                                                                                                                                                                                          | `true` or `false`                                                              |
| `safariOpenLinksInBackground` | (Sim-only) Whether Safari should allow links to open in new windows. Default keeps current sim setting.                                                                                                                                                                                          | `true` or `false`                                                              |
| `keepKeyChains`               | (Sim-only) Whether to keep keychains (Library/Keychains) when appium session is started/finished                                                                                                                                                                                                 | `true` or `false`                                                              |
| `localizableStringsDir`       | Where to look for localizable strings. Default `en.lproj`                                                                                                                                                                                                                                        | `en.lproj`                                                                     |
| `processArguments`            | Arguments to pass to the AUT using instruments                                                                                                                                                                                                                                                   | e.g., `-myflag`                                                                |
| `interKeyDelay`               | The delay, in ms, between keystrokes sent to an element when typing.                                                                                                                                                                                                                             | e.g., `100`                                                                    |
| `showIOSLog`                  | Whether to show any logs captured from a device in the appium logs. Default `false`                                                                                                                                                                                                              | `true` or `false`                                                              |
| `sendKeyStrategy`             | strategy to use to type test into a test field. Simulator default: `oneByOne`. Real device default: `grouped`                                                                                                                                                                                    | `oneByOne`, `grouped` or `setValue`                                            |
| `screenshotWaitTimeout`       | Max timeout in sec to wait for a screenshot to be generated. default: 10                                                                                                                                                                                                                         | e.g., `5`                                                                      |
| `waitForAppScript`            | The ios automation script used to determined if the app has been  launched, by default the system wait for the page source not to be  empty. The result must be a boolean                                                                                                                        | e.g. `true;`, `target.elements().length > 0;`, `$.delay(5000); true;`          |
| `webviewConnectRetries`       | Number of times to send connection message to remote debugger, to get webview. Default: `8`                                                                                                                                                                                                      | e.g., `12`                                                                     |
| `appName`                     | The display name of the application under test. Used to automate backgrounding the app in iOS 9+.                                                                                                                                                                                                | e.g., `UICatalog`                                                              |
| `customSSLCert`               | (Sim only) Add an SSL certificate to IOS Simulator.                                                                                                                                                                                                                                              | e.g.  `-----BEGIN CERTIFICATE-----MIIFWjCCBEKg...` `-----END CERTIFICATE-----` |
| `webkitResponseTimeout`       | (Real device only) Set the time, in ms, to wait for a response from WebKit in a Safari session. Defaults to `5000`                                                                                                                                                                               | e.g., `10000`                                                                  |
| `remoteDebugProxy`            | (Sim only, <= 11.2) If set, Appium sends and receives remote  debugging messages through a proxy on either the local port (Sim only,  <= 11.2) or a proxy on this unix socket (Sim only >= 11.3) instead  of communicating with the iOS remote debugger directly.                                | e.g. `12000` or `"/tmp/my.proxy.socket"`                                       |

#### Importance of Desired Capability

- They help in controling the session request with the server. i.e. on which device the automation should run, iOS or Android, or in case of web app which browser.
- They allows the server to select the most appropriate device to send the automation instruction, in can of more than one devices are connected or Server is acting as Appium Hub. 
- Specify automation conditions such as default timeout, proxy etc. 

### `appPackage`

It is the technical name of the application which is set at the time of application creation as it’s the top level package name under which all the code for the app resides.

### `appActivity`

It refers to the different functionalities that are provided by the app usually in different screens.

### Finding appPackage using `adb`

There are two ways using which we can find the appPackage

#### Using adb package

```bash
adb shell pm list packages -3
```

#### Using adb `mCurrentFocus`

```bash
adb shell dumpsys window windows | grep -E 'mCurrentFocus'
```

**Output:**

```bash
mCurrentFocus=Window{79461b9 u0 com.mayankjohri.demo.mayaappiumtextboxesdemo/com.mayankjohri.demo.mayaappiumtextboxesdemo.MainActivity}
```

In the above output, we can see that the **appPackage** is **"com.mayankjohri.demo.mayaappiumtextboxesdemo"** and full name of the Activity currently focused (**appActivity**) is**"com.mayankjohri.demo.mayaappiumtextboxesdemo.MainActivity"**

#### Using adb `mFocusedApp`

```bash
adb shell dumpsys window windows | grep -E 'mFocusedApp'
```

**Output:**

```bash
mFocusedApp=AppWindowToken{2582b77 token=Token{4260976 ActivityRecord{46eb411 u0 com.mayankjohri.demo.mayaappiumtextboxesdemo/.MainActivity t136}}}
```

## Selector Elements Strategies

Appium provide multiple strategies (defined in `appium.webdriver.common.mobileby.MobileBy`) to  

$$TODO$$

| Strategy                                | Description                                                                                    | iOS                                             | Android                                                    | Function              |
| --------------------------------------- | ---------------------------------------------------------------------------------------------- | ----------------------------------------------- | ---------------------------------------------------------- | --------------------- |
| Accessibility ID                        | Read a unique identifier for a UI element.                                                     | `accessibility-id`                              | `content-desc`                                             | `ACCESSIBILITY_ID`    |
| Class name                              | Element Class                                                                                  | For IOS it is the full name of the XCUI element | For Android it is the full name of the UIAutomator2  class | `CLASS_NAME`          |
| ID                                      | Native element identifier.                                                                     | Name                                            | `resource-id`                                              | `ID`                  |
| Name                                    | Name of web element                                                                            | Name                                            | Name                                                       | `NAME`                |
| XPath                                   | Search the app XML source using xpath (not recommended, has performance issues)                |                                                 |                                                            | `XPATH`               |
| Image                                   | Locate an element by matching it with a base 64 encoded image file                             |                                                 |                                                            | `IMAGE`               |
| Android UiAutomator (UiAutomator2 only) | Use the UI Automator API, in particular the `UiSelector` class to locate elements.             |                                                 |                                                            | `ANDROID_UIAUTOMATOR` |
| Link Text                               | for web element                                                                                |                                                 |                                                            | `LINK_TEXT`           |
| Partial Link Text                       | for web element                                                                                |                                                 |                                                            | `PARTIAL_LINK_TEXT`   |
| IOS UIAutomation                        | When automating an iOS application, Apple’s Instruments framework can be used to find elements |                                                 |                                                            | IOS_UIAUTOMATION      |

As most of the command call sign is similar, we are only going to try few of the methods and leave the others as exercise.

### Finding elements by ID

- `find_element_by_id` \ `find_element(By.ID, "<XPath>")`: 
  
  Use any of the above two to select one element using its ID
  
  ```python
  """
  Identify the element using id Various Text views of the Sample Application
  
  """
  from time import sleep
  from appium import webdriver
  ```

  def set_desired_caps():
  """."""
  desired_caps = {}
  desired_caps['platformName'] = 'Android'
  desired_caps['platformVersion'] = '9'

# Mobile name, check `adb devices`

  desired_caps['deviceName'] = 'emulator-5554'   
  desired_caps['appPackage'] = 'com.mayankjohri.demo.buttonsdemo'
  desired_caps['appActivity'] = '.MainActivity'
  return desired_caps

```
def main():



main()
```

- `find_elements_by_id`:

Under normal condition it is very unlikely that we will have a situation, where we have multiple elements having same id. but might work for badly designed Web page inside WebView.

```python
# Skipped due to bad idea.
```

### Finding elements by class name

- `find_element_by_class_name`/ `find_element(By.CLASS_NAME, "Class_Name")`:  Use any of the above two to select one element using its ClassName. If multiple elements have same class name in that case, only first element is selected.
  
  ```python
  from time import sleep
  from appium import webdriver
  from appium.webdriver.common.mobileby  import MobileBy as MBy
  ```
  
  def set_desired_caps():
  
      """."""
      desired_caps = {}
      desired_caps['platformName'] = 'Android'
      desired_caps['platformVersion'] = '9'
      desired_caps['deviceName'] = 'emulator-5554'
      desired_caps['appPackage'] = 'com.mayankjohri.demo.buttonsdemo'
      desired_caps['appActivity'] = '.MainActivity'
      return desired_caps
  
  def main():
  
      """."""
      try:
          driver = webdriver.Remote('http://localhost:4723/wd/hub',
                                    set_desired_caps())
          ele_name = "android.widget.Switch"
          print("Lets get details of: {ele_name}".format(ele_name=ele_name))
          element = driver.find_element_by_class_name(ele_name)
          print("find_element_by_class_name", ele_name, element.text)
      
          element = driver.find_element(MBy.CLASS_NAME, ele_name)
          print("find_element_by_class_name", ele_name, element.text)
          # Although we have multiple elements with checkboxes
          # but find_element_by_class_name will only return the first
          ele_name = "android.widget.CheckBox"
          element = driver.find_element_by_class_name(ele_name)
          print("find_element_by_class_name", ele_name, element.text)
      
          elements = driver.find_elements_by_class_name(ele_name)
          print("find_element_by_class_name", ele_name)
          for ele in elements:
              print(ele.text)
      except Exception as error:
          print(error)
      finally:
          if 'driver' in locals():
              driver.quit()
          print("Bye Bye")
  
  main()

```
-  `find_elements_by_class_name`: Can be used to identify multiple elements as shown in the above example. 

### Finding elements by name (text)

- `find_element_by_name` / `find_element(By.NAME, <Element_Name>)`, `find_elements_by_name`: **Deprecated**

since https://github.com/appium/appium/releases/tag/v1.5.0 

### Finding elements by AccessibilityId

- `find_element_by_accessibility_id`: For **Android** the `accessibility id` maps to the `content-description` for the element as shown in the below example

```python
from time import sleep
from appium import webdriver
from appium.webdriver.common.mobileby  import MobileBy as MBy


def set_desired_caps():
  """."""
  desired_caps = {}
  desired_caps['platformName'] = 'Android'
  desired_caps['platformVersion'] = '9'
  desired_caps['deviceName'] = 'emulator-5554'
  desired_caps['appPackage'] = 'com.mayankjohri.demo.buttonsdemo'
  desired_caps['appActivity'] = '.MainActivity'
  return desired_caps


def main():
  """."""
  try:
      driver = webdriver.Remote('http://localhost:4723/wd/hub',
                                set_desired_caps())
      ele_name = "Switch button"
      print("Lets get details of: {ele_name}".format(ele_name=ele_name))
      element = driver.find_element_by_accessibility_id(ele_name)
      print("find_element_by_accessibility_id", ele_name, element.text)

      element = driver.find_element(MBy.ACCESSIBILITY_ID, ele_name)
      print("find_element by.accessibility_id:", ele_name, element.text)

      ele_name = "Options"
      element = driver.find_element_by_accessibility_id(ele_name)
      print("find_element_by_accessibility_id", ele_name, element.text)

      elements = driver.find_elements_by_accessibility_id(ele_name)
      print("find_elements_by_accessibility_id", ele_name)
      for ele in elements:
          print(ele.text)
  except Exception as error:
      print(error)
  finally:
      if 'driver' in locals():
          driver.quit()
      print("Bye Bye")
main()
```

- `find_elements_by_accessibility_id`: Returns a list of elements which have same `accessibility_by`

```python
user_email = driver.find_elements_by_accessibility_id("Users Email")
```

### Finding elements by XPath

The Selenium clients have methods for retrieving elements using the `xpath` locator strategy.

- `find_element(By.XPATH, "<XPath>")`: It retrieves element using the `xpath` locator strategy

- `find_elements(By.XPATH, "<XPath>")`:  It retrieve elements using the `xpath` locator strategy

```python
from time import sleep
from appium import webdriver
from appium.webdriver.common.mobileby  import MobileBy as MBy


def set_desired_caps():
    """."""
    desired_caps = {}
    desired_caps['platformName'] = 'Android'
    desired_caps['platformVersion'] = '9'
    desired_caps['deviceName'] = 'emulator-5554'
    desired_caps['appPackage'] = 'com.mayankjohri.demo.buttonsdemo'
    desired_caps['appActivity'] = '.MainActivity'
    return desired_caps


def main():
    """."""
    try:
        driver = webdriver.Remote('http://localhost:4723/wd/hub',
                                  set_desired_caps())
        ele_name = "(//android.widget.CheckBox[@content-desc=\"Options\"])[3]"
        print("Lets get details of: {ele_name}".format(ele_name=ele_name))
        element = driver.find_element_by_xpath(ele_name)
        print("find_element_by_xpath", ele_name, element.text)

        element = driver.find_element(MBy.XPATH, ele_name)
        print("find_element by.accessibility_id:", ele_name, element.text)

        ele_name = "//android.widget.CheckBox[@content-desc=\"Options\"]"
        element = driver.find_element_by_xpath(ele_name)
        print("find_element_by_xpath", ele_name, element.text)

        elements = driver.find_elements_by_xpath(ele_name)
        print("find_elements_by_xpath", ele_name)
        for ele in elements:
            print(ele.text)
    except Exception as error:
        print(error)
    finally:
        if 'driver' in locals():
            driver.quit()
        print("Bye Bye")


main()
```

**TIP**: You can use Appium Inspector to get the XPath for the selected element

![images/by_xpath.png](images/by_xpath.png)

#### XPath

Its a W3C standard defined at [Cover page | xpath | W3C standards and drafts | W3C](https://www.w3.org/TR/xpath/) . Its one of the most used and least understood concept. Its gold if done right else its a recipy for disaster

#### Advantages & Limitations - XPath

- Advantages:
  
  - **Built-in** Functions:  More than 200 built-in functions 
  
  - **Complex Issues**: XPath allows to provide a solution to complicated issues and is very useful for dynamic elements if properly solved.
  
  - **Expressive Query Capabilities**:
  
  - **Navigation flexibility**: 
  
  - **Readability**: Its a two edged sword. :)

- Limitations:
  
  - **Performance Overhead**:
  
  - ****Brittleness****: If not done properly
  
  - **Untidy**: XPat 

#### Tips for XPath:

- **Limit overall xpath dependence**

- **Use Simpler Modular Expressions**

- **Use Stable Anchors like IDs**

#### XPath Termenology

- Atomic Value:

- Nodes
  
  - Parents
  
  - Childrens
  
  - Sibling
  
  - Ancestors
  
  - Desendents

### Syntax

We will take the following code for the examples in this section.

```xml
<books>
    <book name="Lets Learn Python" buy="23">
        <topic>Python</topic>
        <author>Rakesh</author>
        <price>12</price>
    </book>    
    <book buy="22">
        <name>Lets Learn Crystal</name>
        <topic>Nim</topic>
        <author>Rahul</author>
        <price>33</price>
    </book>   
    <book name="Lets Learn Go" buy="34">
        <topic>GoLang</topic>
        <author>Banti J</author>
        <price>53</price>
    </book>    
    <book name="Lets Learn Nim" buy="24">
        <topic>Nim</topic>
        <author>Rahul</author>
        <price>33</price>
    </book>    
</books>
```

#### Selecting Nodes

| Expression  | Description                                                                 |
| ----------- | --------------------------------------------------------------------------- |
| `node_name` | Select all nodes with the name `node_name`                                  |
| `/`         | Selects from the root node                                                  |
| `//`        | Selects decendent nodes that matches the selection                          |
| `.`         | Select the current node, similar to folders in any OS, <br>Example: `dir .` |
| `..`        | Select the parent node, similar to folders in any OS, <br>Example: `dir ..` |
| `@`         | Select attributes                                                           |

#### Predicates

| Path Expression                                   | Result                                                                                                                          | Meaning                                                          |
| ------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------- |
| `//book[3]/author[1]`                             | `Rahul`                                                                                                                         | First Author text from 3rd node                                  |
| `//book[3]/author`                                | `Rahul`                                                                                                                         | Author text from 3rd node                                        |
| `//book[3]`                                       | `<book name="Lets Learn Nim"><br/>        <topic>Nim</topic><br/>        <author>Rahul</author><br/>    </book>`                | Third book Node                                                  |
| `//books[last()]`                                 | `<book name="Lets Learn Nim"><br/> <topic>Nim</topic><br/> <author>Rahul</author><br/> </book>`                                 | Last book Node                                                   |
| `//book[@name="Lets Learn Python"]`               | `<book name="Lets Learn Python"><br/>    <topic>Python<br/>    </topic><br/>    <author>Rakesh<br/>    </author><br/>  </book>` | Book with attribute `name` with value `Lets Learn Python`        |
| `//book[@name]`                                   | Python Rakesh<br/>    GoLang Banti J<br/>    Nim Rahul                                                                          | All `book` nodes with attribute `name`                           |
| `//book[@buy>23]`                                 | GoLang Banti J 53<br/>    Nim Rahul 33                                                                                          |                                                                  |
| `//book[price>23]`                                | Lets Learn Crystal Nim Rahul 33<br/>    GoLang Banti J 53<br/>    Nim Rahul 33                                                  |                                                                  |
| `//book[@buy=23]`                                 | `<book name="Lets Learn Python" buy="23">`                                                                                      |                                                                  |
| `//book[@buy=23]/topic`                           | Python                                                                                                                          |                                                                  |
| `//book[@buy=23]/topic \| //book[@buy=23]/author` | Python<br/>    Rakesh                                                                                                           | Returns `topic` and `author` for book which has `buy` value `23` |
| `//book/topic \| //book/author`                   | Python<br/>    Rakesh<br/>    Nim<br/>    Rahul<br/>    GoLang<br/>    Banti J<br/>    Nim<br/>    Rahul                        | Returns all topics and authors                                   |





#### Axes

#### Operators







### Find Elements by `android_uiautomator`

Appium allows to use the UI Automator API to locate elements. It is one of the most versatile way to identify the elements. But due to its complexity should be used only if there is no other method is left.

- `find_element_by_android_uiautomator` / `find_element(By.ANDROID_UIAUTOMATOR, "<uiautomator>")`

- `find_elements_by_android_uiautomator`
  
  ```python
  # -*- coding: utf-8 -*-
  """
  Tests Various Text views of the Sample Application
  
  """
  from time import sleep
  from appium import webdriver
  from selenium.webdriver.common.keys import Keys
  
  def set_desired_caps():
      """."""
      desired_caps = {}
      desired_caps['platformName'] = 'Android'
      desired_caps['platformVersion'] = '9'
      desired_caps['deviceName'] = 'emulator-5554'
      desired_caps['appPackage'] = 'com.mayankjohri.demo.mayaappiumtextboxesdemo'
      desired_caps['appActivity'] = '.MainActivity'
      return desired_caps
  
  def main():
  """."""
  ids = {
      "multi_line":
          "com.mayankjohri.demo.mayaappiumtextboxesdemo:id/et_multi_line",
      "et_date":
          "com.mayankjohri.demo.mayaappiumtextboxesdemo:id/et_date"
  }
  try:
      driver = webdriver.Remote('http://localhost:4723/wd/hub',
                                set_desired_caps())
      ele_id = ids['et_date']
      print("Lets get the textbox `{}`".format(ele_id))
      element = driver.find_element_by_android_uiautomator(
          'new UiScrollable(new UiSelector().scrollable(true).instance(0))'
          '.scrollIntoView(new UiSelector().resourceId("{}")'
          '.instance(0));'.format(ele_id))
      element.click()
      element.send_keys("15/08/1947")
      print(element.text)
  except Exception as error:
      print(error)
  finally:
      if driver:
          driver.quit()
  ```

```
## Best Practices: Identifying Elements

- `accessibility ID` should be the prefered method of identifying the elements, followed by ID, UIAutomator, XPath & ClassName
- Discuss the locator strategy with Developers, let them know the reasoning & purpose behind it and implement one in which everyone including developers are happy.

## Reference

1. http://appium.io/docs/en/writing-running-appium/caps/
2. http://appium.io/docs/en/advanced-concepts/image-elements/
3. https://developer.android.com/studio/command-line/dumpsys
```

```

```

## Comparison with Popular Appium Locators

| Locator               | Pros                                                | Cons                                                                                |
| --------------------- | --------------------------------------------------- | ----------------------------------------------------------------------------------- |
| ID/Name               | Simple, direct element access                       | Brittle, changes break scripts                                                      |
| Accessibility ID      | Reliable access across platforms                    | Limited elements expose these ids, not mandatory                                    |
| XPath                 | Very versatile query language                       | Overuse causes performance issues                                                   |
| UIAutomator (Android) | Reliable native Android element selection           | Only for Android, limited features compared to full-fledged XPath                   |
| Predicate (iOS)       | Accurate finder of iOS elements exposing attributes | Applicable only for iOS platforms, not as versatile as complete XPath functionality |







## Troubleshooting

### Appium Inspector

- **Appium Server Not Running**:
   If you encounter issues where the Appium server isn't starting 
  correctly, check that Node.js is installed properly and that there are 
  no port conflicts on your machine.
- **Device Not Recognized**:
   If the Appium Inspector doesn't recognize your device, ensure that 
  developer options and USB debugging are enabled on the device and that 
  appropriate drivers (for Android) are installed on your machine.
