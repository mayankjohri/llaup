# User Interaction and controls

In this section we will automate basic user interactions with most of the common mobile controls.

## Texts

Android support various types of text controls such as 

- TextView
- TextEdit
- Password
- Password (numeric)
- E-Mail
- Phone
- Postal Address
- Multiline Text
- Time
- Date
- Number
- Number (Signed)
- Number (Decimal)
- AutoCompleteTextView
- MultiAutoCompleteTextView
- CheckedTextView, etc

We will try to cover few of the common tasks which user can perform on them, such as

- Send Keys
- Clear
- Read Value
- Scroll
- Move the cursor to particular location
- Select All 
- Cut
- Paste

### `send_keys`

It is one of the two main tasks which are performed during automation, it allows to send text to the selected element. 

Basic example is as shown

```python
"""
Automate Email Text views interaction
exp for: send_keys, clear, text
"""
from appium import webdriver

def set_desired_caps():
    """Lists desired capabilities"""
    desired_caps = {}
    desired_caps['platformName'] = 'Android'
    desired_caps['platformVersion'] = '9'
    desired_caps['deviceName'] = 'emulator-5554'
    desired_caps['appPackage'] = 'com.mayankjohri.demo.mayaappiumtextboxesdemo'
    desired_caps['appActivity'] = '.MainActivity'
    return desired_caps

def main():
    """Main Automation Function"""
    try:
        driver = webdriver.Remote('http://localhost:4723/wd/hub',
                                  set_desired_caps())
        print("Lets get the textbox `textView`")
        et_email = "com.mayankjohri.demo.mayaappiumtextboxesdemo:id/et_email"
        element = driver.find_element_by_id(et_email)
        # Read existing text from text box
        print(element.text)
        # Clear any existing text
        element.clear()
        # Send text to textbox
        element.send_keys("arya@world.com")
        print(element.text)
    except Exception as error:
        print(error)
    finally:
        if driver:
            driver.quit()
main()
```

In the above example, we sent the text `arya@world.com` using `send_keys` function. We also used `element.clear` any existing text present and was able to read the text using `element.text` attribute. 

#### ASCII Values for Special Key strokes

Following are few ASCII values for common non-printing keystrokes. They can be used as shown in the below code.

| Key Action | Value |
| ---------- | ----- |
| BACK_SPACE | 8     |
| TAB        | 9     |
| ENTER      | 13    |
| SPACE      | 32    |
| DELETE     | 127   |

```python
driver.long_press_keycode(10)
```

### Multi Line Text

We are going to perform following actions on multi line text

```python

```

Actions class

Robot class

Javascript executor

## Touch actions

### `move_to`

moves the cursor to certain cordinates. 

## `tap`

Taps the selected element

## `press`

Press on the selected element

## `long_press`

long press on the selected element

### `release`

### `move_to`

### `wait`

### `cancel`

## Multi-touch (`MultiAction`) actions

`MultiAction`

## Appium-Specific touch actions

### `driver.tap`

#### `driver.swipe`

Swipe from one point to another point.

#### `driver.zoom`

Zoom in on an element, doing a pinch out operation.

#### `driver.pinch`

Zoom out on an element, doing a pinch in operation.    

### Difference between `click` and `tap`

 `tap()` method belongs to AppiumDriver class while the click() method belongs to the WebDriver class. 

It's better to go for tap() method as they have extracted all mobile native gestures