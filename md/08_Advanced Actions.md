# Advanced Actions

##`TouchAction`

`TouchAction` objects contain a chain of events, which can be executed in sequencial order. It is very useful to replicate a collection of actions needed to be performed on the application. The events available to the `TouchAction` object are:

* press
* release
* moveTo
* tap
* wait
* longPress
* cancel
* perform

### `MultiTouch`

*MultiTouch* objects are collections of TouchActions. Its object only have two methods,

- `add`: It is used to add another TouchAction to the MultiTouch.
- `perform`: when called sends all the TouchAction's to the mobile at the sametime as if they are performed at the same time.

```python

```

## Switching between Native and WebView

```python
current = driver.current_context
context_name = "WEBVIEW"
driver.switch_to.context(context_name)
```

## Application management methods

### Backgrounding an application

```python
driver.background_app(1)
sleep(2)
el = driver.find_element_by_name('Animation')
assertIsNotNone(el)
```

### Checking if an application is installed

```python
assertFalse(self.driver.is_app_installed('sdfsdf'))
assertTrue(self.driver.is_app_installed('com.example.android.apis'))
```

### Installing an application

```python
assertFalse(driver.is_app_installed('io.selendroid.testapp'))
driver.install_app('/Users/mayank/code/python-client/test/apps/selendroid-test-app.apk')
assertTrue(driver.is_app_installed('io.selendroid.testapp'))
```

### Removing an application

```python
assertTrue(driver.is_app_installed('com.example.android.apis'))
driver.remove_app('com.example.android.apis')
assertFalse(driver.is_app_installed('com.example.android.apis'))
```

### Closing and Launching an application

```python
el = driver.find_element_by_name('Animation')
assertIsNotNone(el)
driver.close_app();

try:
    driver.find_element_by_name('Animation')
except Exception as e:
    pass # should not exist

driver.launch_app()
el = driver.find_element_by_name('Animation')
assertIsNotNone(el)
```

### Resetting an application

```python
el = driver.find_element_by_name('App')
el.click()

driver.reset()
sleep(5)

el = driver.find_element_by_name('App')
assertIsNotNone(el)
```

### Start an arbitrary activity

```python
driver.start_activity('com.foo.app', '.MyActivity')
```

### Retrieving application strings

```python
strings = driver.app_strings
```

### Sending a key event to an Android device

```python
# sending 'Home' key event
driver.press_keycode(3)
```

### Retrieving the current running package and activity

```python
package = driver.current_package
assertEquals('com.example.android.apis', package)
```

#### Retrieve a file from the device

```python
data = driver.pull_file('data/local/tmp/strings.json')
strings = json.loads(data.decode('base64', 'strict'))
```

#### Place a file on the device

```python
path = 'data/local/tmp/test_push_file.txt'
data = 'This is the contents of the file to push to the device.'
driver.push_file(path, data.encode('base64'))
```
