# Introduction

**Appium** is an open-source tool for automating tasks performed in

- native (Apps written using the iOS, Android, or Windows SDKs), 
- mobile web (web apps accessed using a default mobile browser, and 
- hybrid applications (Apps which have a wrapper around the "webview" [A native control which allows user to interact with web content within native app])

on iOS mobile, Android mobile, and Windows desktop platforms. 

**Note:** Appium is a **cross-platform** framework, which means that it allows to write tests against multiple platforms (iOS, Android, Windows), using the same API, which enables code reuse between iOS, Android, and Windows testsuites for same application with similar GUI features.

## Appium Philosophy

1. You shouldn't have to recompile your app or modify it in any way in order to automate it.
2. You shouldn't be locked into a specific language or framework to write and run your tests.
3. A mobile automation framework shouldn't reinvent the wheel when it comes to automation APIs.
4. A mobile automation framework should be open source, in spirit and practice as well as in name!

## Mobile Applications

### Native App

A native app is developed specifically for one platform and is build using the platform's native framework. 

### Hybrid App

Hybrid Applications contains components from both Native and Web Controls.
