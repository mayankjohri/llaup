# Mobile Utilities

## Android

### Launch an Android Virtual Device

In the Android Virtual Device Manager, click on the green triangle icon to start the selected "Virtual Device"

### Android Debug Bridge (`ADB`)

**`ADB`** is the command line utility which can be used for various administrative tasks on Android devices. In this section, we will try to cover most of the tasks, which you might useful for you. 

#### Start ADB Server

```bash
$:> adb start-server
```

#### Stop ADB Server

```bash
$:> adb kill-server
```

#### List all Android devices connected

```bash
$:> adb devices
```

#### Know Status of the Device

```bash
$:> adb get-state
```

Above command can be used to find the status of the device. Devices can be in one of the follwoing states 

- offline
- bootloader/recovery
- device

#### Get Device Serial Number

```bash
$:> adb get-serialno
```

or 

```bash
$:> adb shell getprop | grep serialno
```

#### Copy Files from Computer to Phone

```bash
$:> adb push [source] [destination]
```

#### Copy Files from Phone to Computer

```bash
$:> adb pull [source] [destination]
```

#### List all installed 3<sup>rd</sup> party applications

```bash
$:> adb shell pm list packages -3
```

#### List All Android installed Packages

Following command can be used to find all the installed packages.

```bash
# Displays Apk File location & Package Name 
$:> adb shell pm list packages -f
```

**Output**

```bash
package:/system/app/WallpaperBackup/WallpaperBackup.apk=com.android.wallpaperbackup
package:/system/priv-app/BlockedNumberProvider/BlockedNumberProvider.apk=com.android.providers.blockednumber
package:/system/app/QK_Xtime/QK_Xtime.apk=com.qiku.android.xtime
package:/system/app/UserDictionaryProvider/UserDictionaryProvider.apk=com.android.providers.userdictionary
package:/system/priv-app/EmergencyInfo/EmergencyInfo.apk=com.android.emergency
package:/system/priv-app/OpaSuwIntegrationSample/OpaSuwIntegrationSample.apk=com.android.opasuwintegrationsample
package:/data/app/com.sonymobile.androidapp.audiorecorder-1/base.apk=com.sonymobile.androidapp.audiorecorder
package:/system/priv-app/QK_PowerEngine/QK_PowerEngine.apk=com.qiku.powerengine
package:/system/app/QK_FileBrowser/QK_FileBrowser.apk=com.qiku.android.filebrowser
```

or  - TO FIX below code

```bash
$:> adb shell 'pm list packages -f | sed -e \'s/.*=//\' | sort'
```

**Output**

```bash
com.google.android.partnersetup
com.google.android.printservice.recommendation
com.google.android.setupwizard
com.google.android.syncadapters.contacts
com.google.android.tag
com.google.android.tts
com.google.android.videos
com.google.android.webview
com.google.android.youtube
```

#### Get Android Version

```bash
$:> adb shell getprop ro.build.version.release
```

#### Get API Level (SDK Version)

```bash
$:> adb shell getprop ro.build.version.sdk
```

or 

```bash
$:> adb shell getprop | grep sdk
```

#### Get All Properties

```bash
$:> adb shell getprop
```

or

```bash
$:> adb shell cat /system/build.prop
```

#### Get Device Model

```bash
$:> adb shell getprop | grep model
```

#### Get disk(s) encryption state

```bash
$:> adb shell getprop ro.crypto.state
```

#### Install Application

```bash
$:> adb install "path/to/file.apk"
```

if you have multiple devices, following command can also be used.

```bash
$:> adb -s [serial-number] install "path/to/file.apk"
```

#### Uninstall Application

```bash
$:> adb uninstall [package-name]
```

if you have multiple devices, following command can also be used.

```bash
$:> adb -s [serial-number] uninstall [package-name]
```

#### Start Remote Shell

```bash
$:> adb shell
```

#### Running remote scripts

```bash
$:> adb shell <remote script/command>
```

#### Take Screenshots

```bash
$:> adb shell screencap -p "/path/to/screenshot.png"
```

#### Record Android Screen

```bash
$:> adb shell screenrecord "/path/to/record.mp4"
```

#### Log Files

- **Reading**
  
  ```bash
  $:> adb logcat
  ```
  
  Above command prints the logcat of the android device, If you wish to save the log in a file, use the following command
  
  ```bash
  $:> adb logcat > <filename>
  ```

- **Reading log for Package**

```bash
$:> adb logcat <packagename>:[priority level: V, D, I, W, E, F, S]
```

The priority levels of logs:

V – Verbose (lowest priority)
 D – Debug
 I – Info
 W – Warning
 E – Error
 F – Fatal
 S – Silent (highest priority, on which nothing is ever printed)

- **Create a bug report**
  
  ```bash
  $:> adb bugreport > <filename>
  ```

- **Clear log**
  
  ```bash
  $:> adb logcat -c
  ```

#### Start an activity

```bash
$:> adb shell am start -a <activity> -n <application>
```

example:

```bash
 $:> adb shell am start -n com.mayankjohri.demo.dropdownmenus/.LoginActivity
```

#### Start WiFi

```bash
$:> adb shell am broadcast -a io.appium.settings.wifi --es setstatus enable
```

#### Stop WiFI

```bash
$:> adb shell am broadcast -a io.appium.settings.wifi --es setstatus disable
```

#### Enable Data

```bash
$:> adb shell am broadcast -a io.appium.settings.data_connection --es setstatus enable
```

#### Disable Data

```bash
$:> adb shell am broadcast -a io.appium.settings.data_connection --es setstatus disable
```

#### Start Animation

```bash
$:> adb shell am broadcast -a io.appium.settings.animation --es setstatus enable
```

#### Stop Animation

```bash
$:> adb shell am broadcast -a io.appium.settings.animation --es setstatus disable
```

#### Setting Mock Locations

Start sending scheduled updates (every 2s) for mock location with the specified values by executing:
(API versions 26+):

```bash
$:> adb shell am start-foreground-service --user 0 -n io.appium.settings/.LocationService --es longitude {longitude-value} --es latitude {latitude-value} [--es altitude {altitude-value}]
```

For (API Version 26-)

```bash
$:> adb shell am startservice --user 0 -n io.appium.settings/.LocationService --es longitude {longitude-value} --es latitude {latitude-value} [--es altitude {altitude-value}]
```

#### Clean Mock Locations

```bash
$:> adb shell am stopservice io.appium.settings/.LocationService
```

#### Remount the device

```bash
$:> adb remount
```

#### Monkey commands

```bash
$:> adb shell monkey -p <app_id> -v <event_count> -s <repeat_command>
```

#### Grant Permission to application

```bash
$:> adb shell pm grant <app_id> <permission>
```

#### Revoke permission to application

```bash
$:> adb shell pm revoke <app_id> <permission>
```

#### Backup Android Device

```bash
$:> adb backup -all
```

#### Restore Android Device

```bash
$:> adb restore "path/to/backup.adb"
```

#### Reboot Android Device into

- Recovery Mode

```bash
$:> adb reboot-recovery
```

- Bootloader Mode

```bash
$:> adb reboot-bootloader
```

- Fastboot Mode

```bash
$:> adb fastboot
```

#### Runs command on all devices attached using USB

```bash
$:> adb –d <command>
```

#### Runs command on all emulator devices

```bash
$:> adb –e <command>
```

#### Get the current screen res

```bash
$:> adb shell wm size
```

#### ADB port forward

`adb` allows port forwarding, which helps us to forward a request received on  host machine to the android device. Say you want to forward any request you received on hostname port 8000 to android device 5000 port, you can use the following command to achieve it. 

```bash
$:> adb forward <local_port> <device_port>
```

**Example:**

```bash
$:> adb forward tcp:8000 tcp:5000
```

#### ADB port reverse forward

Similar to port forwarding, `adb` can also reverse forward the request. Request send by the application on its port can be forwarded to the host port. 

```bash
$:> adb reverse <device_port> <local_port>
```

**Example:**

```bash
$:> adb reverse tcp:5000 tcp:8000
```

#### ADB Restore

```bash
adb restore <backed_up_file>
```

#### ADB App Cache Clean

```bash
adb shell pm clear com.mayankjohri.demo.dropdownmenus
```



#### ADB Dump

```bash
db shell dumpsys window windows | grep -E 'mCurrentFocus|mFocusedApp'
```

***Output:**

```
mCurrentFocus=Window{40ace62 u0 com.qiku.android.launcher3/com.qiku.android.launcher3.Launcher}
mFocusedApp=AppWindowToken{d9a45d0 token=Token{258d682 ActivityRecord{65f9dcd u0 com.qiku.android.launcher3/.Launcher t1}}}
```



#### Recompile the apk for speed

```
 cmd package compile -m speed-profile 
```