# Installation & Setup

## Install Android Studio and SDK

Install the following softwares 

- Android Studio
- Android SDK 

## Install Appium

### Appium Desktop - Do not use it

**Appium Desktop is unsupported, no longer maintained, and has known security vulnerabilities. It is advised not to use it at all**

#### macOS or Windows / ReactOS

Download the latest version of Appium Desktop from the [releases page](https://github.com/appium/appium-desktop/releases).

### Appium Core

Appium core can be installed using the following commands. `Node.js` is the pre-requisite for it (Check **FAQs** for details at the end of the chapter). 

```bash
npm install -g appium
```

Beta version of appium server can be installed using the following command.

```bash
npm install -g appium@beta
```

## Appium Inspector

Downloiad the Appium Inspector for your OS from https://github.com/appium/appium-inspector/releases.

> ** Note: **
> 
> Click on  "Show all ..." link to view all the download options

![](/home/mayank/.var/app/com.github.marktext.marktext/config/marktext/images/2025-02-26-04-14-56-image.png)

## Appium UIAutomator

Run the following command to install the UIAutomator driver. This is needed to work with android applications.

//  OLD

// $ npm install appium-uiautomator2-driver

```bash
 appium driver install uiautomator2
```

Example:

```bash
$ appium driver install uiautomator2
✔ Checking if 'appium-uiautomator2-driver' is compatible
✔ Installing 'uiautomator2'
ℹ Driver uiautomator2@4.1.0 successfully installed
- automationName: UiAutomator2
- platformNames: ["Android"]
```

## ### Appium doctor

npm install @appium/doctor --location=global

## Install Appium Client Library

It is recommended to create a new virtual environment, using the following process.

### Install Virtual Env

virtualenv is already part of the distribution, based on linux flavor

you still might have to install the `python-pip` package.

 https://docs.python.org/3/library/venv.html

#### Create Project virtualenv

```bash
$:> python3 -m venv venv
```

#### Activate virtualenv

Always activate the virtualenv before starting your automation.

```bash
$:> source venv/bin/activate
```

#### Deactivate virtualenv

once all the tasks are completed, we should deactivate the virtual environment using the below command.

```bash
$:> deactivate
```

### Install Appium Client

Run the following command after you have activated the virtualenv. 

```bash
$:> pip install Appium-Python-Client
```

It will install the appium client for python, which we will be using for Mobile Automation.

If you are not using virtualenv (**highly discouraged**) , please use the following command to install the appium client

```bash
pip install Appium-Python-Client --user
```

It will install appium client in user profile. 

## Setup

### Android

#### Enable developer options on an Android device

Before we can start with Appium we need to enable "developer options" on android devices. 

- Goto Settings -> About phone

- (scroll to) Build number  and tap on it about 7 times and a notice regarding the development options has been enabled should be displayed 

- Also, the development settings should be visible on mobile device.

#### Setting Env Variables

#### Create an Android Virtual Device

Its a good idea to do the testing using Physical devices, but you can also create a Android Virtual Device, using `avdmanager` (through command line) or using Android Studio (Avd Manager), 

1. avdmanager

2. Using Andrid Studio
   
   1. Launch Android Studio
   
   2. Click on "AVD Manager" icon to start the Android Virtual Device
      
      ![AVD Manager icon](images/avd_manager_icon.png)
   
   3. Click on "Create Virtual Device"
   
   4. Select the appropriate category & required device and click on "Next button"
      
      ![select_hardware](images/select_hardware.png)
   
   5. Select the appropriate system images
      
      ![select_hardware](images/select_image.png)
   
   6. Click on Finish button

## Suggesions for device configuration for Appium

- Start fresh:

- Keep only one version of app

- Enable USB Developer Options

- Use Wifi or Mobile Data

- Disable: Please disable the followings options, if possible
  
  - keyboad
    - Auto-Capitalization
    - Auto-Correction
    - Predictive
  - Auto Lock
  - Lock Screen
  - Auto Sync
    - saves the B/W 
  - Animation
    - Window Animation Scale
    - Animator duration scale
    - Transition animation scale

## FAQ's

### How to install `NodeJS`

Follow the instruction at https://nodejs.org/en/download and select your Operating system or download the binary files from the same page at the lower part of the web page.

![](/home/mayank/.var/app/com.github.marktext.marktext/config/marktext/images/2025-03-04-06-21-21-image.png)

- Installing on Linux for currrent user.

```bash
# Download and install nvm:
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.40.1/install.sh | bash

# in lieu of restarting the shell
\. "$HOME/.nvm/nvm.sh"
```

### How do I get my device detected by ADB on Linux

Lets start the with the basics.

####1 Step 1: Ensure "USB debugging is enabled" on android mobile

- Navigate to "Developer Options"

- Check for "USB debuging" checkbox - This should be enabled, otherwise enable it.
* Run `adb devices` command 

```bash
[mayank@mayalap3 ~]$ adb devices
List of devices attached
LMX4308HPZKNCQKBZP      unauthorized
```

* Select the checkbox on the mobile and press ok

* Again run `adb devices` command

```bash
[mayank@mayalap3 ~]$ adb devices
List of devices attached
LMX4308HPZKNCQKBZP      device
```

>  **Note**:
> 
> This should resolve the issue in most of the cases, but on some linux you might have to perform following steps

#### Step 2: Setting the rules for adb

##### Install `android-tools`

1. Install android-tools which will supply ADB (`sudo dnf install android-tools`)
2. Copy udev rules (`sudo cp /usr/share/doc/android-tools/51-android.rules /etc/udev/rules.d`)
3. Reload udev rules by rebooting or using linux distribution specific command

If the above steps do not work then try the following steps

##### Identify the device using `lsusb` command

Run the `lsusb` and seach for your mobile 

```bash
[mayank@mayalap3 ~]$ lsusb
Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
Bus 001 Device 002: ID 8087:8000 Intel Corp. Integrated Rate Matching Hub
Bus 002 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
Bus 002 Device 002: ID 046d:c52b Logitech, Inc. Unifying Receiver
Bus 002 Device 005: ID 0bda:0129 Realtek Semiconductor Corp. RTS5129 Card Reader Controller
Bus 002 Device 011: ID 0cf3:3004 Qualcomm Atheros Communications AR3012 Bluetooth 4.0
Bus 002 Device 057: ID 046d:0a45 Logitech, Inc. 960 Headset
Bus 002 Device 060: ID 1004:633e LG Electronics, Inc. LM-X420xxx/G2/G3 Android Phone (MTP/download mode)
Bus 003 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub
```

From the above i can see the following line is of interest for me.

```bash
ID 1004:633e LG Electronics, Inc. LM-X420xxx/G2/G3 Android Phone (MTP/download mode)
```

You can use `usb-devices` command also to cross verify the `Vendor` and `ProdID` 

```bash
[mayank@mayalap3 ~]$ usb-devices 

T:  Bus=01 Lev=00 Prnt=00 Port=00 Cnt=00 Dev#=  1 Spd=480  MxCh= 2
D:  Ver= 2.00 Cls=09(hub  ) Sub=00 Prot=00 MxPS=64 #Cfgs=  1
P:  Vendor=1d6b ProdID=0002 Rev=06.12
S:  Manufacturer=Linux 6.12.13_1 ehci_hcd
S:  Product=EHCI Host Controller
S:  SerialNumber=0000:00:1d.0
C:  #Ifs= 1 Cfg#= 1 Atr=e0 MxPwr=0mA
I:  If#= 0 Alt= 0 #EPs= 1 Cls=09(hub  ) Sub=00 Prot=00 Driver=hub
E:  Ad=81(I) Atr=03(Int.) MxPS=   4 Ivl=256ms

T:  Bus=01 Lev=01 Prnt=01 Port=00 Cnt=01 Dev#=  2 Spd=480  MxCh= 8
D:  Ver= 2.00 Cls=09(hub  ) Sub=00 Prot=01 MxPS=64 #Cfgs=  1
P:  Vendor=8087 ProdID=8000 Rev=00.04
C:  #Ifs= 1 Cfg#= 1 Atr=e0 MxPwr=0mA
I:  If#= 0 Alt= 0 #EPs= 1 Cls=09(hub  ) Sub=00 Prot=00 Driver=hub
E:  Ad=81(I) Atr=03(Int.) MxPS=   2 Ivl=256ms

T:  Bus=02 Lev=00 Prnt=00 Port=00 Cnt=00 Dev#=  1 Spd=480  MxCh= 9
D:  Ver= 2.00 Cls=09(hub  ) Sub=00 Prot=01 MxPS=64 #Cfgs=  1
P:  Vendor=1d6b ProdID=0002 Rev=06.12
S:  Manufacturer=Linux 6.12.13_1 xhci-hcd
S:  Product=xHCI Host Controller
S:  SerialNumber=0000:00:14.0
C:  #Ifs= 1 Cfg#= 1 Atr=e0 MxPwr=0mA
I:  If#= 0 Alt= 0 #EPs= 1 Cls=09(hub  ) Sub=00 Prot=00 Driver=hub
E:  Ad=81(I) Atr=03(Int.) MxPS=   4 Ivl=256ms

T:  Bus=02 Lev=01 Prnt=01 Port=00 Cnt=01 Dev#=  2 Spd=12   MxCh= 0
D:  Ver= 2.00 Cls=00(>ifc ) Sub=00 Prot=00 MxPS= 8 #Cfgs=  1
P:  Vendor=046d ProdID=c52b Rev=12.11
S:  Manufacturer=Logitech
S:  Product=USB Receiver
C:  #Ifs= 3 Cfg#= 1 Atr=a0 MxPwr=98mA
I:  If#= 0 Alt= 0 #EPs= 1 Cls=03(HID  ) Sub=01 Prot=01 Driver=usbhid
E:  Ad=81(I) Atr=03(Int.) MxPS=   8 Ivl=8ms
I:  If#= 1 Alt= 0 #EPs= 1 Cls=03(HID  ) Sub=01 Prot=02 Driver=usbhid
E:  Ad=82(I) Atr=03(Int.) MxPS=   8 Ivl=2ms
I:  If#= 2 Alt= 0 #EPs= 1 Cls=03(HID  ) Sub=00 Prot=00 Driver=usbhid
E:  Ad=83(I) Atr=03(Int.) MxPS=  32 Ivl=2ms

T:  Bus=02 Lev=01 Prnt=02 Port=01 Cnt=01 Dev#= 60 Spd=480  MxCh= 0
D:  Ver= 2.00 Cls=00(>ifc ) Sub=00 Prot=00 MxPS=64 #Cfgs=  2
P:  Vendor=1004 ProdID=633e Rev=04.09
S:  Manufacturer=LGE
S:  Product=LM-X430
S:  SerialNumber=LMX4308HPZKNCQKBZP
C:  #Ifs= 2 Cfg#= 1 Atr=80 MxPwr=500mA
I:  If#= 0 Alt= 0 #EPs= 3 Cls=06(still) Sub=01 Prot=01 Driver=(none)
E:  Ad=01(O) Atr=02(Bulk) MxPS= 512 Ivl=0ms
E:  Ad=81(I) Atr=02(Bulk) MxPS= 512 Ivl=0ms
E:  Ad=82(I) Atr=03(Int.) MxPS=  28 Ivl=4ms
I:  If#= 1 Alt= 0 #EPs= 2 Cls=ff(vend.) Sub=42 Prot=01 Driver=usbfs
E:  Ad=02(O) Atr=02(Bulk) MxPS= 512 Ivl=0ms
E:  Ad=83(I) Atr=02(Bulk) MxPS= 512 Ivl=0ms

T:  Bus=02 Lev=01 Prnt=60 Port=02 Cnt=01 Dev#= 57 Spd=12   MxCh= 0
D:  Ver= 1.10 Cls=00(>ifc ) Sub=00 Prot=00 MxPS=64 #Cfgs=  1
P:  Vendor=046d ProdID=0a45 Rev=01.00
S:  Manufacturer=Logitech
S:  Product=Logitech USB Headset
S:  SerialNumber=000000000000
C:  #Ifs= 4 Cfg#= 1 Atr=80 MxPwr=100mA
I:  If#= 0 Alt= 0 #EPs= 0 Cls=01(audio) Sub=01 Prot=00 Driver=snd-usb-audio
I:  If#= 1 Alt= 0 #EPs= 0 Cls=01(audio) Sub=02 Prot=00 Driver=snd-usb-audio
I:  If#= 2 Alt= 0 #EPs= 0 Cls=01(audio) Sub=02 Prot=00 Driver=snd-usb-audio
I:  If#= 3 Alt= 0 #EPs= 1 Cls=03(HID  ) Sub=00 Prot=00 Driver=usbhid
E:  Ad=83(I) Atr=03(Int.) MxPS=   4 Ivl=32ms

T:  Bus=02 Lev=01 Prnt=57 Port=03 Cnt=01 Dev#=  5 Spd=480  MxCh= 0
D:  Ver= 2.00 Cls=ff(vend.) Sub=ff Prot=ff MxPS=64 #Cfgs=  1
P:  Vendor=0bda ProdID=0129 Rev=39.60
S:  Manufacturer=Generic
S:  Product=USB2.0-CRW
S:  SerialNumber=20100201396000000
C:  #Ifs= 1 Cfg#= 1 Atr=a0 MxPwr=500mA
I:  If#= 0 Alt= 0 #EPs= 3 Cls=ff(vend.) Sub=06 Prot=50 Driver=rtsx_usb
E:  Ad=01(O) Atr=02(Bulk) MxPS= 512 Ivl=0ms
E:  Ad=82(I) Atr=02(Bulk) MxPS= 512 Ivl=0ms
E:  Ad=83(I) Atr=03(Int.) MxPS=   3 Ivl=64ms

T:  Bus=02 Lev=01 Prnt=05 Port=06 Cnt=01 Dev#= 11 Spd=12   MxCh= 0
D:  Ver= 1.10 Cls=e0(wlcon) Sub=01 Prot=01 MxPS=64 #Cfgs=  1
P:  Vendor=0cf3 ProdID=3004 Rev=00.02
C:  #Ifs= 2 Cfg#= 1 Atr=e0 MxPwr=100mA
I:  If#= 0 Alt= 0 #EPs= 3 Cls=e0(wlcon) Sub=01 Prot=01 Driver=btusb
E:  Ad=02(O) Atr=02(Bulk) MxPS=  64 Ivl=0ms
E:  Ad=81(I) Atr=03(Int.) MxPS=  16 Ivl=1ms
E:  Ad=82(I) Atr=02(Bulk) MxPS=  64 Ivl=0ms
I:  If#= 1 Alt= 0 #EPs= 2 Cls=e0(wlcon) Sub=01 Prot=01 Driver=btusb
E:  Ad=03(O) Atr=01(Isoc) MxPS=   0 Ivl=1ms
E:  Ad=83(I) Atr=01(Isoc) MxPS=   0 Ivl=1ms

T:  Bus=03 Lev=00 Prnt=00 Port=00 Cnt=00 Dev#=  1 Spd=5000 MxCh= 4
D:  Ver= 3.00 Cls=09(hub  ) Sub=00 Prot=03 MxPS= 9 #Cfgs=  1
P:  Vendor=1d6b ProdID=0003 Rev=06.12
S:  Manufacturer=Linux 6.12.13_1 xhci-hcd
S:  Product=xHCI Host Controller
S:  SerialNumber=0000:00:14.0
C:  #Ifs= 1 Cfg#= 1 Atr=e0 MxPwr=0mA
I:  If#= 0 Alt= 0 #EPs= 1 Cls=09(hub  ) Sub=00 Prot=00 Driver=hub
E:  Ad=81(I) Atr=03(Int.) MxPS=   4 Ivl=256ms
```

So, in my case I received `Vendor=1004 ProdID=633e` as reqired values.

##### Creating the rule for adb

>  **Note:**
> 
> You need to be have `sudo` permission to achieve it.

Open/Create the `/etc/udev/rules.d/51-android.rules` file and add the following file (after changing the `idVendor` and `idProduct` values)

`SUBSYSTEM=="usb", ATTR{idVendor}=="1004", ATTR{idProduct}=="633e", MODE="0666", GROUP="plugdev", SYMLINK+="android%n"`

##### Add user to `plugdev` group

Check if the user is part of `plugdev` group by running the `groups` command

```bash
[mayank@mayalap3 ~]$ groups
mayank kmem wheel ... tty tape kvm input users
```

Add user to `plugdev` group, again you will need `sudo` permission to achieve it.

```bash
sudo usermod -a -G plugdev $USER
```

Please relogin for the changes to take effect and check again by running the groups command 

```bash
[mayank@mayalap3 ~]$ groups
mayank kmem wheel tty ... tape kvm input plugdev users
```

> **<mark>Note</mark>**:
> 
> If nothing works, change the USB cable and it might help :)

### Common Errors

- driver not installed
  
  ```bash
  $ appium
  [Appium] Welcome to Appium v2.16.2 (REV 179d45050be0a71fd57591b0ed8aedf9b177ba10)
  [Appium] The autodetected Appium home path: /home/mayank/.appium
  [Appium] Appium REST http interface listener started on http://0.0.0.0:4723
  [Appium] You can provide the following URLs in your client code to connect to this server:
      http://127.0.0.1:4723/ (only accessible from the same host)
      http://192.168.0.227:4723/
  [Appium] No drivers have been installed in /home/mayank/.appium. Use the "appium driver" command to install the one(s) you want to use.
  [Appium] No plugins have been installed. Use the "appium plugin" command to install the one(s) you want to use.
  ```

- ANDROID_HOME missing
  
  ![](/home/mayank/.var/app/com.github.marktext.marktext/config/marktext/images/2025-03-04-08-49-42-image.png)

- 

## References

1. Environment variables: [https://developer.android.com/tools/variables](https://developer.android.com/tools/variables)