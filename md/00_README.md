# README

This versio of book only covers Android automation with Appium using Python Language.

### Who is this book for?

- **QA engineer**, who wish to explore the world of mobile automation using **Appium** using **Python Language**
- **QA Architect**, who wish to write mobile/unified automation frameworks
- **Developers** , who wish to automate certain aspects of the application for evaluating their work

### Assumption

It is assumed that reader has

- Basic understanding of Programming in Python Language, you can read my other book, "Lets explore Core Python" (https://www.amazon.com/dp/B079W9FCHL)
- Basic understanding of Mobile Testing
- A computer with Linux, mac or Windows OS installed
- Atleast one android / iOS mobile with all required softwares installed

### Version History

| Version | Date       | Comments                              |
| ------- | ---------- | ------------------------------------- |
| 0.0.1   | 09-02-2019 | Initial Version                       |
| 0.0.2   | 31-12-2020 | Updated with latest version of Appium |
| 0.0.3   | 10-03-2025 | Updated with latest version of Appium |

## NOTE: !!! Please read me !!!

As this is still one of the initial versions, few topics might not be covered or partially covered. Please let me know the topics which you would like to be added or expanded at (mayankjohri@gmail.com) and I will try to cover them in future versions. If you have purchased the current version, future updates can be downloaded for free though amazon website (as long as amazon provides this feature. You can always talk to amazon help-desk to get the latest version pushed to your device).  

### Very helpful links

- Display and control your Android device https://github.com/Genymobile/scrcpy
