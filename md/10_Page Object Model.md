# Page Object Model and Page Factory



Feature Comparision

| Feature         | Plain                      | POM       |
| :-------------- | :------------------------- | --------- |
| Test Logic & UI | Integrated in one file     | Separated |
| Maintenance     | Difficult                  | Ease      |
| Core Redundancy | Redundant                  | No        |
| Complexity      | Complex and disjoined code | Easy      |

## Design

In POM we seperate Business Logic, UI Objects and Testcases in seperate files/classes. The UI Objects are aligned based on the pages on which they appear, business logic contains just the business operations (such as login, sell, buy, add items to bucket etc) and they do not contains any test logic. Test cases utilises the business Logic to validate the business actions as part of testing. 

## Steps

### Identify & Mark Testcases

Before we start writing the automation test cases, we need to write and analyse the testcases. Seperate them in different sections based on business demand, we also need to mark the priority of testcases based on its business needs. 

### Evaluate Testcases

Pick testcases which cover most of the conditions (about 70% of testcases) and leave 30% for manual testing in normal scenario's.

### Create & deploy the testing framework

Create & use testing frame based on testing requirement. 

### Create Page Objects

In this secion we will identify the elements using various locator strategies which we learned in chapter "Basics of Appium". A Sample example is shown below.

```python
from appium.webdriver.common.mobileby  import MobileBy as MBy

one_button = MBy.ID, "com.mayankjohri.demo.dragdrop:id/iv_one"
sec_button = My.ID, "com.mayankjohri.demo.dragdrop:id/iv_two"
```

In the above example, we have two elements, which are identified using their respective ID values. The variable stores both the strategy and elements respective value, which allows appium to uniquely identify the element/elements. 

### Create Business Logic

Once all the elements required have been identified, we can start creating building blocks of business logic. First we need to identify & divide tasks into multiple smallest independent tasks which can be executed. Business Logic should not have any test logic in it and should not have any validation.

### Create Test Cases

 Once the business logic is created we can now create the testcases using any framework we want. 