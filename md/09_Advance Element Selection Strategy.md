# Advance Element Selection Strategy

## Android UIAutomator search

### UIAutomator 2.0 API

- [By](https://developer.android.com/reference/android/support/test/uiautomator/By.html) – a utility class which enables the creation of BySelector.
- [BySelector](https://developer.android.com/reference/android/support/test/uiautomator/BySelector.html) – Specifies a criteria for matching UI elements during a call to findObject(…)
- [UiCollection](https://developer.android.com/reference/android/support/test/uiautomator/UiCollection.html) –  This class is used to enumerate UI elements for the purpose of  counting, or targeting a sub elements by a child’s text or description.
- [UiObject](https://developer.android.com/reference/android/support/test/uiautomator/UiObject.html)  – A UiObject contains information to help it locate a matching view at  runtime based on the UiSelector properties specified in its constructor.  Once UiObject instance is created it can be reused for different views  that match the selector criteria.
- [UiObject2](https://developer.android.com/reference/android/support/test/uiautomator/UiObject2.html)  – Represents a UI element. This is slightly different from UiObject as  UiObject2 elements can be used even an underlying view object is  terminated.
- [UiScrollable](https://developer.android.com/reference/android/support/test/uiautomator/UiScrollable.html) – UiScrollable is a UiCollection and provides support for searching for items in scrollable layout elements.
- [UiSelector](https://developer.android.com/reference/android/support/test/uiautomator/UiSelector.html)  – This specifies the UI elements in the layout hierarchy for tests to  target, filtered by properties such as text value, content-description,  class name, and state information, among others.
- [Configurator](https://developer.android.com/reference/android/support/test/uiautomator/Configurator.html) – Allows to set parameters for running UIAutomator tests and those settings will impact immediately.

#### BySelector

| Public methods             | Description |
| -------------------------- | ------------------------------------------------------------ |
| `BySelector` | `checkable(boolean isCheckable) ` <br><br>Sets the search criteria to match elements that are checkable or not checkable. |
| `BySelector` | `checked(boolean isChecked)       `  <br><br>Sets the search criteria to match elements that are checked or unchecked. |
| `BySelector` | `clazz(String packageName, String className)       `<br><br>Sets the class name criteria for matching. |
| `BySelector` | `clazz(Class clazz)` <br>Sets the class name criteria for matching. |
| `BySelector` | `clazz(Pattern className)` <br>Sets the class name criteria for matching. |
| `BySelector` | `clazz(String className)` <br>Sets the class name criteria for matching. |
| `BySelector` | `clickable(boolean isClickable)` <br>Sets the search criteria to match elements that are clickable or not clickable. |
| `BySelector` | `depth(int min, int max)` <br>Sets the search criteria to match elements that are in a range of depths. |
| `BySelector` | `depth(int exactDepth)` <br>Sets the search criteria to match elements that are at a certain depth. |
| `BySelector` | `desc(String contentDescription)` <br>Sets the content description criteria for matching. |
| `BySelector` | `desc(Pattern contentDescription)` <br>Sets the content description criteria for matching. |
| `BySelector` | `descContains(String substring)` <br>Sets the content description criteria for matching. |
| `BySelector` | `descEndsWith(String substring)` <br>Sets the content description criteria for matching. |
| `BySelector` | `descStartsWith(String substring)` <br>Sets the content description criteria for matching. |
| `BySelector` | `enabled(boolean isEnabled)` <br>Sets the search criteria to match elements that are enabled or disabled. |
| `BySelector` | `focusable(boolean isFocusable)` <br>Sets the search criteria to match elements that are focusable or not focusable. |
| `BySelector` | `focused(boolean isFocused)` <br>Sets the search criteria to match elements that are focused or unfocused. |
| `BySelector` | `hasChild(BySelector childSelector)` Adds a child selector criteria for matching. |
| `BySelector` | `hasDescendant(BySelector descendantSelector)` Adds a descendant selector criteria for matching. |
| `BySelector` | `hasDescendant(BySelector descendantSelector, int maxDepth)` Adds a descendant selector criteria for matching. |
| `BySelector` | `longClickable(boolean isLongClickable)` <br>Sets the search criteria to match elements that are long clickable or not long clickable. |
| `BySelector` | `maxDepth(int max)` <br>Sets the search criteria to match elements that are no more than a certain depth. |
| `BySelector` | `minDepth(int min)` <br>Sets the search criteria to match elements that are at least a certain depth. |
| `BySelector` | `pkg(Pattern applicationPackage)` <br>Sets the package name criteria for matching. |
| `BySelector` | `pkg(String applicationPackage)` <br>Sets the application package name criteria for matching. |
| `BySelector` | `res(String resourceName)` <br>Sets the resource name criteria for matching. |
| `BySelector` | `res(String resourcePackage, String resourceId)` <br>Sets the resource name criteria for matching. |
| `BySelector` | `res(Pattern resourceName)` <br>Sets the resource name criteria for matching. |
| `BySelector` | `scrollable(boolean isScrollable)` <br>Sets the search criteria to match elements that are scrollable or not scrollable. |
| `BySelector` | `selected(boolean isSelected)` <br>Sets the search criteria to match elements that are selected or not selected. |
| `BySelector` | `text(Pattern textValue)` <br>Sets the text value criteria for matching. |
| `BySelector` | `text(String textValue)` <br>Sets the text value criteria for matching. |
| `BySelector` | `textContains(String substring)` <br>Sets the text value criteria for matching. |
| `BySelector` | `textEndsWith(String substring)` <br>Sets the text value criteria for matching. |
| `BySelector` | `textStartsWith(String substring)` <br>Sets the text value criteria for matching. |
| `String`     | `toString()` Returns a `String` representation of this `BySelector`. |

## iOS UIAutomation search

$$TODO$$

## iOS class chain

$$TODO$$



## References:

- https://developer.android.com/reference/android/support/test/uiautomator/UiSelector
- https://facebook.github.io/react-native/docs/accessibility#sending-accessibility-events-android
- http://appium.io/docs/en/advanced-concepts/image-elements/